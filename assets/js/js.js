$("#form_brand").submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function (data) {
			var args = $.parseJSON(data);
			if (args['status_valid'] == 'false') {
				$(".help-block").remove();
				$('input[name=nama_brand]').after(args['err_nama_brand']);
				$('input[name=keterangan]').after(args['err_keterangan']);
			} else {
				if (args['msg'] != '') {
					alert(args['msg']);
				}
			}
		}
	});
	return false;
});

$("#form_kategori").submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function (data) {
			var args = $.parseJSON(data);
			if (args['status_valid'] == 'false') {
				$(".help-block").remove();
				$('input[name=nama_kategori]').after(args['err_nama_kategori']);
				$('input[name=keterangan]').after(args['err_keterangan']);
			} else {
				if (args['msg'] != '') {
					alert(args['msg']);
				}
			}
		}
	});
	return false;
});

$("#form_barang").submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function (data) {
			var args = $.parseJSON(data);
			if (args['status_valid'] == 'false') {
				$(".help-block").remove();
				$('input[name=kode_barang]').after(args['err_kode_barang']);
				$('input[name=nama_barang]').after(args['err_nama_barang']);
				$('select[name=brand]').after(args['err_brand']);
				$('select[name=kategori]').after(args['err_kategori']);
				$('textarea[name=detail_barang]').after(args['err_detail_barang']);
			} else {
				if (args['msg'] != '') {
					alert(args['msg']);
				}
			}
		}
	});
	return false;
});

$("#form_produk").submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function (data) {
			var args = $.parseJSON(data);
			if (args['status_valid'] == 'false') {

				$(".help-block").remove();
				$('input[name=kode_produk]').after(args['err_kode_produk']);
				$('input[name=nama_produk]').after(args['err_nama_produk']);
				$('select[name=paket_tipe]').after(args['err_paket_tipe']);
				$('input[name=harga]').after(args['err_harga']);
				$('textarea[name=deskripsi]').after(args['err_deskripsi']);
			} else {
				if (args['msg'] != '') {
					alert(args['msg']);
				}
			}
		}
	});
	return false;
});

$("#form_customer").submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function (data) {
			var args = $.parseJSON(data);
			if (args['status_valid'] == 'false') {
				$(".help-block").remove();
				//   $('input[name=nama_barang]').after(args['err_nama_barang']);
				//   $('select[name=brand]').after(args['err_brand']);
				//   $('select[name=kategori]').after(args['err_kategori']);
				//   $('textarea[name=detail_barang]').after(args['err_detail_barang']);
			} else {
				if (args['msg'] != '') {
					alert(args['msg']);
				}
			}
		}
	});
	return false;
});

$("#form_sewa").submit(function (event) {
	event.preventDefault();
	var objSewa={};
	objSewa.kode_rental      = $("#kode_rental").val();
	objSewa.customer_id      = $("#customer_id_add option:selected").val();
	objSewa.project_name     = $("#project_name").val();
	objSewa.tanggal_mulai    = $("#tanggal_mulai").val();
	objSewa.tanggal_selesai  = $("#tanggal_selesai").val();
	objSewa.keterangan       = $("#keterangan").val();
	var arrProduk=[];

	$(".produk_list_sewa tr").each(function() {
		var tempItem={};
		tempItem.id=this.id.slice(5);
		tempItem.value=$("#prod_jlh_"+tempItem.id).val();
		arrProduk.push(tempItem);
	});
	
	arrProduk = arrProduk.filter(function( obj ) {
		return obj.id !== '';
	});

	if(arrProduk.length>0){
		objSewa.items=arrProduk;
	}else{
		objSewa.items=0;
	}

	
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: objSewa,
		success: function (data) {
			// console.log(data);
			var args = $.parseJSON(data);
			if (args['status_valid'] == 'false') {
				$(".help-block").remove();
				$('#kode_rental').after(args['err_kode_rental']);
				$('#customer_id_add').after(args['err_customer_id']);
				$('#project_name').after(args['err_project_name']);
				$('#tanggal_mulai').after(args['err_tanggal_mulai']);
				$('#tanggal_selesai').after(args['err_tanggal_selesai']);
				$('#keterangan').after(args['err_keterangan']);
			} else {
				if (args['msg'] != '') {
					alert(args['msg']);
					if(args['status_message']){

					}
				}
			}
		}
	});
	return false;
});


$("#form_inv").submit(function (event) {
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function (data) {
			var args = $.parseJSON(data);
			// console.log(data);
			if (args['status_valid_pilih_tipe'] == 'false') {

				$(".help-block").remove();
				$('select[name=tipe_bayar]').after(args['err_tipe_bayar']);
			} else {
				if (args['status_valid_dp'] == 'false') {
					$('input[name=dp_bayar]').after(args['err_dp_bayar']);
				}else{
					if (args['msg'] != '') {
						alert(args['msg']);
					}
				}
				if (args['status_valid_pelunasan'] == 'false') {
					$('input[name=pelunasan]').after(args['err_pelunasan']);
				}else{
					if (args['msg'] != '') {
						alert(args['msg']);
					}
				}
			}
		}
	});
	return false;
});


$(document).ready(function () {

	$('#tanggal_mulai').datepicker();
	$('#tanggal_selesai').datepicker();

	$(".barang_list").on('click', '.deleteBtnProduct', function () {
		var trid = $(this).closest('tr').attr('id');
		remove_barang(trid);
		$(this).closest('tr').remove();
	});

	$(".produk_list_sewa").on('click', '.deleteBtnProductSewa', function () {
		var trid = $(this).closest('tr').attr('id');
		remove_p_barang(trid);
		$(this).closest('tr').remove();
	});
	

	$('.addBarang').click(add_barang);
	$('.addProduk').click(add_produk);

	function barang_in_product() {
		//Ajax here  
		//   $(this).parents('tr').remove();
		$(this).closest('tr').remove();
	}

	function add_barang() {
		//---------------------------------------
		var val = $('select[name=barang_add] option:selected').val()
		var pathArray = window.location.pathname.split('/');
		var url = "addProdukBarang";
		if (pathArray[2] === 'produk' && pathArray[3] === 'edit') {
			url = "../addProdukBarang";
		}

		$.ajax({
			type: 'POST',
			url: url,
			data: { barang: val },
			dataType: "json",
			success: function (data) {
				if (data == '0') {
					alert('Sudah di list');
				} else {
					var texts = $('select[name=barang_add] option:selected').text()
					var mytext = texts.split("|");

					$('.barang_list').append('<tr id=' + val + '><td>' + mytext[0] + '</td><td>' + mytext[1] + '</td><td>' + mytext[2].replace(/Brand|:/g, '') + '</td><td>' + mytext[3].replace(/Kategori|:/g, '') + '</td><td><a class="deleteBtnProduct">X</a></td></tr>');
				}
			},
			failure: function (errMsg) {
				alert(errMsg);
			}
		});
	}

	function add_produk() {
		//---------------------------------------
		var val = $('select[name=prod_add] option:selected').val()
		var pathArray = window.location.pathname.split('/');
		var url = "addSewaProduk";
		if (pathArray[2] === 'sewa' && pathArray[3] === 'edit') {
			url = "../addSewaProduk";
		}

		$.ajax({
			type: 'POST',
			url: url,
			data: { produk: val },
			dataType: "json",
		}).done(function (data) {
			if (data.msg == '0') {
				alert('Sudah di list');
			} else {
				$('.produk_list_sewa').append('<tr id="prod_' + val + '"><td>' + data.produk.kode_produk + '</td><td>' +  data.produk.deskripsi  + '</td><td>' +  data.produk.keterangan + '</td><td>' + (data.produk.package==1? 'SET':'PCS') + '</td><td>' +  data.produk.harga + '</td><td> <input id="prod_jlh_'+val+'" type="number" class="form-control" require>  </td><td><a class="btn  btn-light font-weight-bold detailBarangSewa">Detail Barang</a>   <a class="ml-3 deleteBtnProductSewa font-weight-bold">X</a><input id="prod_detail_' + val + '" type="hidden" value="'+JSON.stringify(data.produk.detail).replace(/"/g, "'")+'"></td></tr>');
			}
		}).fail(function (errMsg) {
			alert(errMsg);
		})
	}

	function remove_barang(trId) {
		var pathArray = window.location.pathname.split('/');
		var url = "removeProdukBarang";
		if (pathArray[2] === 'produk' && pathArray[3] === 'edit') {
			url = "../removeProdukBarang";
		}
		$.ajax({
			type: 'POST',
			url: url,
			data: { trId: trId },
			dataType: "json",
			success: function (data) {
				console.log(data)
			},
			failure: function (errMsg) {
				alert(errMsg);
			}
		});
	}

	function remove_p_barang(trid) {
		var pathArray = window.location.pathname.split('/');
		var url = "removeProduk";
		if (pathArray[2] === 'sewa' && pathArray[3] === 'edit') {
			url = "../removeProduk";
		}
		$.ajax({
			type: 'POST',
			url: url,
			data: { trId: trid },
			dataType: "json",
			success: function (data) {
				console.log(data)
			},
			failure: function (errMsg) {
				alert(errMsg);
			}
		});
	}


	$('.addCust').click(addCust);
	function addCust() {
		//---------------------------------------
		var val = $('select[name=customer_id] option:selected').val()
		var pathArray = window.location.pathname.split('/');
		var url = "addCustomerSewa";
		if (pathArray[2] === 'sewa' && pathArray[3] === 'edit') {
			url = "../addCustomerSewa";
		}

		$.ajax({
			type: 'POST',
			url: url,
			data: { cust: val },
			dataType: "json",
			success: function (data) {

				$('#kode_customer').html(data.customer.kode_customer);
				$('#nama_customer').html(data.customer.nama_customer);
				$('#no_hp').html(data.customer.no_hp);
				$('#nama_perusahaan').html(data.customer.nama_perusahaan);
				$('#no_telp').html(data.customer.no_telp);
				$('#email').html(data.customer.email);
				$('#alamat').html(data.customer.alamat_tinggal);
				$('#alamat_kantor').html(data.customer.alamat_kantor);
				$('#alamat_perusahaan').html(data.customer.alamat_perusahaan);
				$('#pasport_id').html(data.customer.paspport_id);
				$('#npwp').html(data.customer.npwp);
				$('#jenis_industri_perusahaan').html(data.customer.jenis_industri_perusahaan);


				if (data.customer.tipe_customer == 1) {
					$('#type_customer').html("Perusahaan");
				} else if (data.customer.tipe_customer == 2) {
					$('#type_customer').html("Pribadi");
				} else {
					$('#type_customer').html("Lainnya");
				}


				// 1 Perusahaan
				// 2 Pribadi
				// 3 Lain


			},
			failure: function (errMsg) {
				alert(errMsg);
			}
		});
	}
});

// Add barang in produk
$(document).ready(function () {
	$('.js-example-basic-single').select2(
		{
			theme: "bootstrap",
			placeholder: 'Cari Barang',
			ajax: {
				url: base_url + 'barang/select_barang',
				dataType: 'json',
				delay: 250,
				// method:'POST',
				processResults: function (data) {
					return {
						results: data
					};
				},
				cache: true
			}
		}
	);

	$('.js-example-basic-single2').select2(
		{
			theme: "bootstrap",
			placeholder: 'Cari Customer',
			ajax: {
				url: base_url + 'customer/select_customer',
				dataType: 'json',
				delay: 250,
				// method:'POST',
				processResults: function (data) {
					return {
						results: data
					};
				},
				cache: true
			}
		}
	);

	$('.js-example-basic-single3').select2(
		{
			theme: "bootstrap",
			placeholder: 'Cari Produk',
			ajax: {
				url: base_url + 'produk/select_produk',
				dataType: 'json',
				delay: 250,
				// method:'POST',
				processResults: function (data) {
					return {
						results: data
					};
				},
				cache: true
			}
		}
	);

	$('#tipe_bayar').on('change', function() {
		// alert( this.value );
		if(this.value==1){
			$("#pelunasan").prop('disabled', true);
			$("#dp_bayar").prop('disabled', false);
		}else if(this.value==2){
			$("#pelunasan").prop('disabled', false);
			$("#dp_bayar").prop('disabled', true);
		}
	});

})

$(document).on('click', '.detailBarangSewa', function(e) {
	// function detailBarangs(){
				// e.preventDefault();
		trid = $('tr').attr('id'); // table row ID 
		var trid = $(this).closest('tr');
		if ($('#detail-'+trid.attr('id')).length > 0) { 
			$('#detail-'+trid.attr('id')).remove();
		}else{
			var tables='';
			var trow='';
			var tend='';
			var objDetailItem=$('#prod_detail_'+trid.attr('id').slice(5)).val();
			objDetailItem=JSON.parse(objDetailItem.replace(/'/g, '"'));
			console.log(objDetailItem);
			tables="<table class='table  table-bordered test'>"+
			"<thead>"+
			"<tr>"+
			"<th>Kode Barang</th>"+
			"<th>Nama Barang</th>"+
			"<th>Brand</th>"+
			"<th>Kategori</th>"+
			"</tr>"+
			"</thead>"+
			"<tbody>";
			for (let index = 0; index < objDetailItem.length; index++) {
				let element = objDetailItem[index];
			
					// for (let k in element) {
						trow+="<tr class='table-light'>"+
							"<td>"+element["kode_barang"]+"</td>"+
							"<td>"+element["nama_barang"]+"</td>"+
							"<td>"+element["nama_brand"]+"</td>"+
							"<td>"+element["nama_kategori"]+"</td>"+
						"</tr>";
					// }
				
			}
			tend="</tbody>"+
			"</table>";
			trid.after('<tr id="detail-'+trid.attr('id')+'" style="background-color: #FFFF;"><td colspan="7"> <h5class="mt-2 bm-2">Barang In Produk</h5>'+tables+trow+tend+'</td></tr>');

		}

});

$(document).on('click', '.detail-sewa', function(e) {
	var id = $(this).attr('id');
	var trid = $(this).closest('tr');
	
	if ($('#detail-list-'+id).length > 0) { 
		$('#detail-list-'+id).remove();
	}else{
		$.ajax({
			type: 'POST',
			url: base_url + 'sewa/detailRental',
			data: { uuid_rental: id },
			dataType: "json",
		}).done(function (data) {
			var tables='';
			var trow='';
			var tend='';
			tables="<table class='table  table-bordered test'>"+
				"<thead>"+
					"<tr>"+
						"<th>Kode Produk</th>"+
						"<th>Nama Produk</th>"+
						"<th>Harga</th>"+
						"<th>Jumlah</th>"+
						"<th>Total Harga</th>"+
						"<th></th>"+
					"</tr>"+
				"</thead>"+
			"<tbody>";
			var produkRental=data.produk.rental;
			for (let index = 0; index < produkRental.length; index++) {
				let element = produkRental[index];
					trow+="<tr class='table-light'>"+
						"<td>"+element["kode_produk"]+"</td>"+
						"<td>"+element["nama_produk"]+"</td>"+
						"<td>"+element["harga"]+"</td>"+
						"<td>"+element["jumlah"]+"</td>"+
						"<td>"+element["jumlah"]*element["harga"]+"</td>"+
						"<td><a class='btn btn-light font-weight-bold d-barang'>Detail Barang</a></td>"+
					"</tr>";
			}
			tend="</tbody>"+
			"</table>";
			trid.after('<tr id="detail-list-'+id+'" style="background-color: #FFFF;"><td colspan="10"><div class="p-3"><h5 class="mt-2 bm-2">Detail Rental</h5>'+tables+trow+tend+'</div></td></tr>');

		}).fail(function (errMsg) {
			alert(errMsg);
		})
	}
});

$(document).on("click",".d-barang", function () {
	alert('adsad');
});

function myFunction(){
	alert("asadsd");
}
