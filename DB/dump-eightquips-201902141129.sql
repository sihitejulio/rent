-- MySQL dump 10.13  Distrib 5.7.23, for Win64 (x86_64)
--
-- Host: localhost    Database: eightquips
-- ------------------------------------------------------
-- Server version	5.7.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `uuid_barang` varchar(100) NOT NULL,
  `kode_barang` varchar(15) DEFAULT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `uuid_brand` varchar(100) DEFAULT NULL,
  `uuid_kategori` varchar(100) DEFAULT NULL,
  `info_detail` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`uuid_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES ('6f8d4aef-23ca-4927-a7a9-dda0a6a079ff','KD120001','Kamera','139c5fbc-8af5-455f-a1b9-e529181655b1','43ce85d0-966f-4f69-9788-f2ce365584dd','Test 2  ','2019-01-08 03:39:51','2019-01-08 15:03:24',1),('6f8d4aef-23ca-4927-a7a9-dda0f6a079ff','KD120002','Kamera','139c5fbc-8af5-455f-a1b9-e529181655b1','43ce85d0-966f-4f69-9788-f2ce365584dd','Test 2  ','2019-01-08 03:39:51','2019-01-08 15:03:24',1),('d08a743d-d456-4df2-a8d4-2aa213bc7d5a','BRG-003','Test','7681785d-fcaa-4187-8395-6b0d6304f20d','43ce85d0-966f-4f69-9788-f2ce365584dd','  1','2019-01-17 20:08:24','2019-01-17 20:08:24',1);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `uuid_brand` varchar(100) NOT NULL,
  `kode_brand` varchar(15) DEFAULT NULL,
  `nama_brand` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`uuid_brand`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES ('1046bdb1-ebf4-438f-ac1f-1869f9166899',NULL,'Test','12121','2019-02-12 18:36:01','2019-02-12 18:36:01',1),('139c5fbc-8af5-455f-a1b9-e529181655b1',NULL,'qweqe','12313','2019-01-02 20:33:12','0000-00-00 00:00:00',1),('417f92f2-c3e4-48eb-85de-3826d2a8bab2',NULL,'asada','asda','2019-01-02 20:37:23','2019-01-02 20:37:23',1),('7681785d-fcaa-4187-8395-6b0d6304f20d',NULL,'wsdad','asdada','2019-01-02 20:37:35','2019-01-02 20:37:35',1),('7aab2bef-3d4a-4f4a-b91f-606820254e59',NULL,'qweqe','21323','2019-01-02 20:37:17','2019-01-02 20:37:17',1),('7e88e7a4-3013-4339-a921-870e9b3397b6',NULL,'12313','123131','2019-01-02 20:36:56','2019-01-02 20:36:56',1),('966dfe3d-7615-456a-9a0a-9844bf112e07',NULL,'Test','123','2019-01-18 01:53:18','2019-01-18 01:53:18',1),('9916fd63-f2f3-45b0-98ee-6ebce9e7ca92',NULL,'12313','13232','2019-01-02 20:36:19','2019-01-02 20:36:19',1);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `uuid_customer` varchar(100) NOT NULL,
  `kode_customer` varchar(10) DEFAULT NULL,
  `nama_customer` varchar(100) DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `no_telp` varchar(14) DEFAULT NULL,
  `tipe_customer` varchar(2) DEFAULT NULL COMMENT '1 Perusahaan\r\n2 Pribadi\r\n3 Lain\r\n',
  `alamat_tinggal` text,
  `alamat_kantor` text,
  `pasport_id` varchar(100) DEFAULT NULL,
  `npwp` varchar(100) DEFAULT NULL,
  `nama_perusahaan` varchar(100) DEFAULT NULL,
  `alamat_perusahaan` text,
  `jenis_industri_perusahaan` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES ('51285ec0-a415-4d6b-a87e-4b0fbcc15410','CUST-0001','Raden','0862121212','raden@gmail.com','12131231','2','  Medan  ','Medan  ','12121','12121','W',' -  ','-','2019-01-13 12:32:13','2019-02-12 18:35:42');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail_produk`
--

DROP TABLE IF EXISTS `detail_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail_produk` (
  `uuid_detail_produk` varchar(100) NOT NULL,
  `uuid_produk` varchar(100) DEFAULT NULL,
  `uuid_barang` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid_detail_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail_produk`
--

LOCK TABLES `detail_produk` WRITE;
/*!40000 ALTER TABLE `detail_produk` DISABLE KEYS */;
INSERT INTO `detail_produk` VALUES ('05613230-613c-4303-9894-a9fb443d499a','b5b84f78-1ff9-4081-9600-baf2e19ec4e7','d08a743d-d456-4df2-a8d4-2aa213bc7d5a','2019-02-05 18:31:18','2019-02-05 18:31:18'),('6232bfd5-fcaa-4bb1-a428-23a1d5179b59','b5b84f78-1ff9-4081-9600-baf2e19ec4e7','6f8d4aef-23ca-4927-a7a9-dda0f6a079ff','2019-02-05 18:31:18','2019-02-05 18:31:18');
/*!40000 ALTER TABLE `detail_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail_rental`
--

DROP TABLE IF EXISTS `detail_rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail_rental` (
  `uuid_rental` varchar(100) NOT NULL,
  `uuid_produk` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `jumlah` decimal(19,0) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail_rental`
--

LOCK TABLES `detail_rental` WRITE;
/*!40000 ALTER TABLE `detail_rental` DISABLE KEYS */;
INSERT INTO `detail_rental` VALUES ('fbd8caa7-7eec-43dd-8726-168a39eb6aed','b5b84f78-1ff9-4081-9600-baf2e19ec4e7','2019-02-14 04:32:18','2019-02-14 04:32:18',1,NULL);
/*!40000 ALTER TABLE `detail_rental` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `uuid_inv` varchar(100) NOT NULL,
  `kode_inv` varchar(30) DEFAULT NULL,
  `uuid_rental` varchar(100) NOT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dp_bayar` decimal(19,0) DEFAULT NULL,
  `total_bayar` decimal(19,0) DEFAULT NULL,
  `status_inv` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES ('4e15a658-de2e-4842-a904-a20a338e01df','INV-000000001','5515dc6e-f448-4e88-83a2-e9a26ce16d09','Dp Bayar','2019-02-14 04:10:29','2019-02-14 04:11:05',10000,1010000,1),('c1f86ad6-7bed-4c1e-ab06-c572bc21d980','INV-000000002','fbd8caa7-7eec-43dd-8726-168a39eb6aed','  -','2019-02-14 04:32:54','2019-02-14 04:32:54',10000,NULL,1);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `uuid_kategori` varchar(100) NOT NULL,
  `nama_kategori` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`uuid_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES ('43ce85d0-966f-4f69-9788-f2ce365584dd','Test-Kategori 1','Test','2019-01-06 17:50:32','2019-01-06 17:55:54',1),('60fb7f5f-4cc0-45c3-ad6c-aa3c2c740f88','Radsad','wqeqw','2019-02-12 18:36:28','2019-02-12 18:36:28',1);
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produk` (
  `uuid_produk` varchar(100) NOT NULL,
  `kode_produk` varchar(30) DEFAULT NULL,
  `nama_produk` varchar(100) DEFAULT NULL,
  `package` varchar(100) DEFAULT NULL COMMENT 'pcs/ set',
  `deskripsi` text,
  `harga` decimal(19,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`uuid_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produk`
--

LOCK TABLES `produk` WRITE;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` VALUES ('b5b84f78-1ff9-4081-9600-baf2e19ec4e7','PRD-0001','Produk Set','1','   Ini  ',1000000,'2019-01-17 20:13:30','2019-02-05 18:31:18',1);
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental`
--

DROP TABLE IF EXISTS `rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rental` (
  `uuid_rental` varchar(100) NOT NULL,
  `kode_rental` varchar(20) DEFAULT NULL,
  `uuid_customer` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status_rent` int(1) DEFAULT NULL COMMENT '1:pending,2:inv_dp,3:inv_lunas,5:pengembalian',
  `nama_project` text,
  `status_surat_jalan` int(1) DEFAULT NULL COMMENT '1:surat_jalan,',
  PRIMARY KEY (`uuid_rental`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental`
--

LOCK TABLES `rental` WRITE;
/*!40000 ALTER TABLE `rental` DISABLE KEYS */;
INSERT INTO `rental` VALUES ('fbd8caa7-7eec-43dd-8726-168a39eb6aed','RENT-000000001','51285ec0-a415-4d6b-a87e-4b0fbcc15410','-','2019-02-14','2019-02-28','2019-02-14 04:32:18','2019-02-14 04:32:18',1,'Hanya Test',NULL);
/*!40000 ALTER TABLE `rental` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surat_jalan`
--

DROP TABLE IF EXISTS `surat_jalan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surat_jalan` (
  `uuid_surat_jalan` varchar(100) NOT NULL,
  `uuid_inv` varchar(100) NOT NULL,
  `nama_brand` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`uuid_surat_jalan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surat_jalan`
--

LOCK TABLES `surat_jalan` WRITE;
/*!40000 ALTER TABLE `surat_jalan` DISABLE KEYS */;
/*!40000 ALTER TABLE `surat_jalan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'eightquips'
--

--
-- Dumping routines for database 'eightquips'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-14 11:29:54
