<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Categorie extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        $this->load->model('kategori_model');
    }

    public function index(){
        $data['title']         = 'Eightquips Kategori';
        $data['title_content'] = 'List Kategori';
        $data['contents']      = 'categorie/list_kategori';
        // List kategori
        $data['kategoris']     =  $this->kategori_model->_get_all();
        $this->load->view('main/app', $data);
    }

    public function add(){
        $data['title']         = 'Eightquips Kategori';
        $data['title_content'] = 'Add Kategori';
        $data['contents']      = 'categorie/form_kategori';
       
        $this->load->view('main/app', $data);
    }

    public function save(){
        $jsonresult = array();

        $this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
      
        if ($this->form_validation->run() == false) {
			$jsonresult['status_valid'] = 'false';
            $jsonresult['err_nama_kategori']   = form_error('nama_kategori','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_keterangan']   = form_error('keterangan','<div class="help-block" style="color:red;">', '</div>');
        } else {
            $uuid              = $this->input->post('uuid_kategori');
            $kategori             = $this->input->post('nama_kategori');
            $keteranga_kategori   = $this->input->post('keterangan');
            $message           = '';
                if(!isset($uuid)){
                            $data_kategori=array(
                                'uuid_kategori'  => $this->gen_uuid->generate_uuid(),
                                'nama_kategori'  => $kategori,
                                'keterangan'  => $keteranga_kategori,
                                'created_at'  => date('Y-m-d H:i:s'),
                                'updated_at'  => date('Y-m-d H:i:s'),
                                'status'      => '1'// 1: aktif
                            );
                            $add_kategori=$this->kategori_model->_insert($data_kategori);
                            // After Insert
                            if($add_kategori){
                                $message='Kategori added successfully.';
                            }else{
                                $message='Kategori gagal insert.';
                            }
                }else{
                            $data_kategori['wheres']=array('uuid_kategori'        => $uuid);
                            $data_kategori['updates']=array(
                                'nama_kategori'  => $kategori,
                                'keterangan'  => $keteranga_kategori,
                                'updated_at'   =>  date('Y-m-d H:i:s')
                            );
                            $update_kategori=$this->kategori_model->_update($data_kategori);
                             // After Insert
                            if($update_kategori){
                                $message='Kategori update successfully.';
                            }else{
                                $message='Kategori gagal update.';
                            }

                }
                $jsonresult['msg']=$message;
            }
		echo json_encode($jsonresult);
    }

    public function edit(){
        $data['title']         = 'Eightquips kategori';
        $data['title_content'] = 'Edit Kategori';
        $kategori_id=$this->input->get('categorie_id');
        // Check exists ID kategori
        if(isset($kategori_id)){
            $data['contents']      = 'categorie/form_kategori';
            $data['kategori']      =  $this->kategori_model->_get_where(array('uuid_kategori'=>$kategori_id))->row_array();
        }

        $this->load->view('main/app', $data);
    }
}