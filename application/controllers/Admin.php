<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        // $this->load->model('admin_model');
    }

    public function index(){
        $data['title']         = 'Eightquips Admin';
        $data['title_content'] = 'List Admin';
        $data['contents']      = 'admin/list_admin';
        // $data['customer']      =  $this->customer_model->_get_all();

        $this->load->view('main/app', $data);
    }

    public function add(){
        // $data['title']         = 'Eightquips Customer';
        // $data['title_content'] = 'Add Customer';
        // $data['contents']      = 'customer/form_customer';
        // $data['auto_customer_code']= $this->customer_model->_max_id();
       
        // $this->load->view('main/app', $data);
    }

    // public function save(){
    //     $jsonresult = array();

    //     $this->form_validation->set_rules('kode_customer', 'Kode Customer', 'required');
    //     $this->form_validation->set_rules('nama_customer', 'Nama Customer', 'required');
    //     $this->form_validation->set_rules('tipe_customer', 'Tipe Customer', 'required');
    //     $this->form_validation->set_rules('no_telp', 'No Telp', 'required');
    //     $this->form_validation->set_rules('no_hp', 'No Hp', 'required');
    //     $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    //     $this->form_validation->set_rules('pasport_id', 'Pasport Id', 'required');
    //     $this->form_validation->set_rules('npwp', 'NPWP', 'required');
      
    //     if ($this->form_validation->run() == false) {
	// 		$jsonresult['status_valid'] = 'false';
    //         $jsonresult['err_kode_customer']   = form_error('kode_customer','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_nama_customer']   = form_error('nama_customer','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_tipe_customer']   = form_error('tipe_customer','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_no_telp']         = form_error('no_telp','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_no_hp']           = form_error('no_hp','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_email']           = form_error('email','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_pasport_id']      = form_error('pasport_id','<div class="help-block" style="color:red;">', '</div>');
    //         $jsonresult['err_npwp']            = form_error('npwp','<div class="help-block" style="color:red;">', '</div>');
    //     } else {
    //         $uuid                 = $this->input->post('uuid_customer');
    //         $nama_customer        = $this->input->post('nama_customer');
    //         $kode_customer        = $this->input->post('kode_customer');
    //         $tipe_customer        = $this->input->post('tipe_customer');
    //         $no_telp              = $this->input->post('no_telp');
    //         $no_hp                = $this->input->post('no_hp');
    //         $email                = $this->input->post('email');
    //         $pasport_id           = $this->input->post('pasport_id');
    //         $npwp                 = $this->input->post('npwp');
    //         $alamat_tinggal       = $this->input->post('alamat_tinggal');
    //         $alamat_kantor        = $this->input->post('alamat_kantor');
    //         $nama_perusahaan      = $this->input->post('nama_perusahaan');
    //         $alamat_perusahaan    = $this->input->post('alamat_perusahaan');
    //         $jenis_perusahaan    = $this->input->post('jenis_perusahaan');

    //         $message           = '';
    //         if(!isset($uuid)){
    //                 $data_customer=array(
    //                     'uuid_customer'              => $this->gen_uuid->generate_uuid(),
    //                     'nama_customer'              => $nama_customer,
    //                     'kode_customer'              => $kode_customer,
    //                     'tipe_customer'              => $tipe_customer,
    //                     'no_hp'                      => $no_hp,
    //                     'email'                      => $email,
    //                     'no_telp'                    => $no_telp,
    //                     'pasport_id'                 => $pasport_id,
    //                     'npwp'                       => $npwp,
    //                     'alamat_tinggal'             => $alamat_tinggal,
    //                     'alamat_kantor'              => $alamat_kantor,
    //                     'nama_perusahaan'            => $nama_perusahaan,
    //                     'alamat_perusahaan'          => $alamat_perusahaan,
    //                     'jenis_industri_perusahaan'  => $jenis_perusahaan,
    //                     'created_at'                 => date('Y-m-d H:i:s'),
    //                     'updated_at'                 => date('Y-m-d H:i:s')
    //                 );
    //                 $add_customer=$this->customer_model->_insert($data_customer);
    //                 // After Insert
    //                 if($add_customer){
    //                     $message='Customer added successfully.';
    //                 }else{
    //                     $message='Customer gagal insert.';
    //                 }
    //         }else{
    //                 $data_customer['wheres']=array('uuid_customer'        => $uuid);
    //                 $data_customer['updates']=array(
    //                     'nama_customer'              => $nama_customer,
    //                     'kode_customer'              => $kode_customer,
    //                     'tipe_customer'              => $tipe_customer,
    //                     'no_hp'                      => $no_hp,
    //                     'email'                      => $email,
    //                     'no_telp'                    => $no_telp,
    //                     'pasport_id'                 => $pasport_id,
    //                     'npwp'                       => $npwp,
    //                     'alamat_tinggal'             => $alamat_tinggal,
    //                     'alamat_kantor'              => $alamat_kantor,
    //                     'nama_perusahaan'            => $nama_perusahaan,
    //                     'alamat_perusahaan'          => $alamat_perusahaan,
    //                     'jenis_industri_perusahaan'  => $jenis_perusahaan,
    //                     'updated_at'   =>  date('Y-m-d H:i:s')
    //                 );
    //                 $update_customer=$this->customer_model->_update($data_customer);
    //                 // After Insert
    //                 if($update_customer){
    //                     $message='Customer update successfully.';
    //                 }else{
    //                     $message='Customer gagal update.';
    //                 }
    //         }
    //         $jsonresult['msg']=$message;
    //     }

	// 	echo json_encode($jsonresult);
    // }

    // public function edit(){
    //     $data['title']         = 'Eightquips Customer';
    //     $data['title_content'] = 'Edit Customer';
    //     $customer_id=$this->input->get('customer_id');
    //     // Check exists ID Customer
    //     if(isset($customer_id)){
    //         $data['contents']     = 'customer/form_customer';
    //         $data['customer']        =  $this->customer_model->_get_where(array('uuid_customer'=>$customer_id))->row_array();
    //     }

    //     $this->load->view('main/app', $data);
    // }

    // public function select_customer()
    // {
    //     $json = [];
    //     $code_search = $this->input->get('q');
    //     $data = $this->customer_model->get_customer($code_search);
    //     foreach ($data as $row) {
    //         $json[] = ['id'=>$row->uuid_customer, 'text'=>$row->kode_customer."|".$row->nama_customer];
    //     }

    //     echo json_encode($json);
    // }
}