<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Brand extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        $this->load->model('brand_model');
    }

    public function index(){
        $data['title']         = 'Eightquips Brand';
        $data['title_content'] = 'List Brand';
        $data['contents']      = 'brand/list_brand';
        // List brand
        $data['brands']        =  $this->brand_model->_get_all();
        $this->load->view('main/app', $data);
    }

    public function add(){
        $data['title']         = 'Eightquips Brand';
        $data['title_content'] = 'Add Brand';
        $data['contents']      = 'brand/form_brand';
       
        $this->load->view('main/app', $data);
    }

    public function save(){
        $jsonresult = array();

        $this->form_validation->set_rules('nama_brand', 'Nama Brand', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
      
        if ($this->form_validation->run() == false) {
			$jsonresult['status_valid'] = 'false';
            $jsonresult['err_nama_brand']   = form_error('nama_brand','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_keterangan']   = form_error('keterangan','<div class="help-block" style="color:red;">', '</div>');
        } else {
            $uuid              = $this->input->post('uuid_brand');
            $brand             = $this->input->post('nama_brand');
            $keteranga_brand   = $this->input->post('keterangan');
            $message           = '';
                if(!isset($uuid)){
                            $data_brand=array(
                                'uuid_brand'  => $this->gen_uuid->generate_uuid(),
                                'nama_brand'  => $brand,
                                'keterangan'  => $keteranga_brand,
                                'created_at'  => date('Y-m-d H:i:s'),
                                'updated_at'  => date('Y-m-d H:i:s'),
                                'status'      => '1'// 1: aktif
                            );
                            $add_brand=$this->brand_model->_insert($data_brand);
                            // After Insert
                            if($add_brand){
                                $message='Brand added successfully.';
                            }else{
                                $message='Brand gagal insert.';
                            }
                }else{
                            $data_brand['wheres']=array('uuid_brand'        => $uuid);
                            $data_brand['updates']=array(
                                'nama_brand'  => $brand,
                                'keterangan'  => $keteranga_brand,
                                'updated_at'   =>  date('Y-m-d H:i:s')
                            );
                           $update_brand=$this->brand_model-->_update($data_brand);
                            // After Insert
                           if($update_brand){
                               $message='Brand update successfully.';
                           }else{
                               $message='Brand gagal update.';
                           }
                }
                $jsonresult['msg']=$message;
            }
		echo json_encode($jsonresult);
    }

    public function edit(){
        $data['title']         = 'Eightquips Brand';
        $data['title_content'] = 'Edit Brand';
        $brand_id=$this->input->get('brand_id');
        // Check exists ID Brand
        if(isset($brand_id)){
            $data['contents']      = 'brand/form_brand';
            $data['brand']        =  $this->brand_model->_get_where(array('uuid_brand'=>$brand_id))->row_array();
        }

        $this->load->view('main/app', $data);
    }
}