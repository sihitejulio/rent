<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sewa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        $this->load->model('sewa_model');
        $this->load->model('customer_model');
        $this->load->model('produk_model');
    }

    public function index(){
        $data['title'] = 'Eightquips Sewa';
        // $data['subtitle'] = 'ppob';
        $data['title_content'] = 'List Sewa';
        $data['contents'] = 'sewa/list_sewa';
        $data['list_sewa']     =  $this->sewa_model->_get_all_join();

        $this->load->view('main/app', $data);
    }

    public function add(){
        $this->session->unset_userdata('add_sewa_produk');

        $data['title']         = 'Eightquips Sewa';
        $data['title_content'] = 'Add Sewa';
        $data['contents']      = 'sewa/form_sewa';
        $data['auto_rental_code']= $this->sewa_model->_max_id();
        $this->load->view('main/app', $data);
    }

    public function addCustomerSewa(){
        $cust      = $this->input->post('cust');
        $data['customer']        =  $this->customer_model->_get_where(array('uuid_customer'=>$cust))->row_array();
        echo json_encode($data);


    }

    public function save(){
        $jsonresult = array();

        $this->form_validation->set_rules('customer_id', 'Customer ID', 'required');
        $this->form_validation->set_rules('kode_rental', 'Kode Rental', 'required');
        $this->form_validation->set_rules('project_name', 'Nama Project', 'required');
        $this->form_validation->set_rules('tanggal_mulai', 'Tanggal Selesai', 'required');
        $this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

      
        if ($this->form_validation->run() == false) {
            
            $jsonresult['status_valid'] = 'false';
            $jsonresult['err_kode_rental']      = form_error('kode_rental','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_customer_id']      = form_error('customer_id','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_project_name']     = form_error('project_name','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_tanggal_mulai']    = form_error('tanggal_mulai','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_tanggal_selesai']  = form_error('tanggal_selesai','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_keterangan']       = form_error('keterangan','<div class="help-block" style="color:red;">', '</div>');
        } else {
            if($this->input->post('items')!=0){
                $uuid            = $this->input->post('uuid_rental');
                $uuid_customer   = $this->input->post('customer_id');
                $project_name    = $this->input->post('project_name');
                $tanggal_mulai   = $this->input->post('tanggal_mulai');
                $tanggal_selesai = $this->input->post('tanggal_selesai');
                $kode_rental     = $this->input->post('kode_rental');
                $keterangan      = $this->input->post('keterangan');
                $items      = $this->input->post('items');

                $info_value_false=FALSE;

                foreach ($items as $key) {
                   if($key['value']==null ||$key['value']==''){
                        $info_value_false=TRUE;
                   }
                }
                if($info_value_false==FALSE){
                    $message           = '';
                    if(!isset($uuid)){
                        $data_rental=array(
                            'uuid_rental'   => $this->gen_uuid->generate_uuid(),
                            'kode_rental'   => $kode_rental,
                            'nama_project'  => $project_name,
                            'uuid_customer' => $uuid_customer,
                            'tgl_mulai'     => date('Y-m-d',strtotime($tanggal_mulai)),
                            'tgl_selesai'   => date('Y-m-d',strtotime($tanggal_selesai)),
                            'keterangan'    => $keterangan,
                            'created_at'    => date('Y-m-d H:i:s'),
                            'updated_at'    => date('Y-m-d H:i:s'),
                            'status_rent'        => '1'// 1: pending
                        );
                        
                        $add_sewa=$this->sewa_model->_insert($data_rental);
                        if($add_sewa){
                            foreach ($items as $key) {
                                $data_detail_rent=array(
                                    'uuid_rental'        => $data_rental['uuid_rental'],
                                    'uuid_produk'        => $key['id'],
                                    'jumlah'             => $key['value'],
                                    'created_at'         => date('Y-m-d H:i:s'),
                                    'updated_at'         => date('Y-m-d H:i:s')
                                );
                                $add_detail_rent=$this->sewa_model->_insert_detail($data_detail_rent);
                            }
                            $message='Rental added successfully.';
                            $jsonresult['status_message']=0;
                        }else{
                            $message='Rental gagal insert.';
                        }
                    }
                    $jsonresult['msg']=$message;

                }else{
                    $jsonresult['msg']='Jumlah Produk Tidak Boleh Kosong';
                }
            }else{
                $jsonresult['msg']='Produk Masih Belum Di Pilih';
            }
        }
		echo json_encode($jsonresult);
    }

    // Barang Manage
    public function addSewaProduk(){
        $prodAdd      = $this->input->post('produk');
        $data           = array('list'=>array());
        // Add array
        $msg='0';
        $result=[];
        
        $listProd=$this->session->userdata('add_sewa_produk');
        if(isset($listProd)){
            $data   = $listProd;
            if(!isset($data['list'][$prodAdd])){
                $data['list'][$prodAdd]=1;
                $this->session->set_userdata('add_sewa_produk',$data);
                // search
                $result['produk'] =  $this->produk_model->_get_where(array('uuid_produk'=>$prodAdd))->row_array();
                $result['produk']['detail'] =  $this->produk_model->_get_barang_where(array('detail_produk.uuid_produk'=>$prodAdd))->result_array();

                $result['msg']    =  1;
            }else{
                $result['msg']    =  0;

            }
        }else{
            // array_push($data['list'],$barangAdd);
            $data['list'][$prodAdd]=1;
            $this->session->set_userdata('add_sewa_produk',$data);
            $result['produk'] =  $this->produk_model->_get_where(array('uuid_produk'=>$prodAdd))->row_array();
            $result['produk']['detail'] =  $this->produk_model->_get_barang_where(array('detail_produk.uuid_produk'=>$prodAdd))->result_array();
            // echo '1';
            $result['msg']    =  1;          

        }
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }

    public function removeProduk(){
        $id     = $this->input->post('trId');
        $listProdBarang = $this->session->userdata('add_sewa_produk');
        unset($listProdBarang['list'][substr($id,5)]);
        $this->session->set_userdata('add_sewa_produk',$listProdBarang);
        // print_r($id);
        print_r($listProdBarang);
    } 

    public function detailRental(){
        $uuid_rent                  =  $this->input->post('uuid_rental');
        $result['rental']           =  $this->sewa_model->_get_where(array('uuid_rental'=>$uuid_rent))->row_array();
        $result['produk']['rental'] =  $this->sewa_model->_get_produk_where(array('detail_rental.uuid_rental'=>$uuid_rent))->result_array();
        $result['produk']['detail'] =  $this->sewa_model->_get_detail_where(array('detail_rental.uuid_rental'=>$uuid_rent))->result_array();
            
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($result));
    }


    public function detail(){

        $data['uuid_rental']  = $this->uri->segment('3');


        $data['title']         = 'Eightquips Sewa';
        $data['title_content'] = 'Add Sewa';
        $data['contents']      = 'sewa/form_sewa';
        $data['info_client']   =  $this->sewa_model->_get_all_join(array('uuid_rental'=> $data['uuid_rental']))->row_array();
        $data['produk_detail'] =  $this->sewa_model->_get_all_join_produk(array('rental.uuid_rental'=> $data['uuid_rental']));
        // $data['produk_detail'] = 'sewa/form_sewa';

        // $data['auto_rental_code']= $this->sewa_model->_max_id();
        $this->load->view('sewa/detail_surat_jalan', $data);
    }

}