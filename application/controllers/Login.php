<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $data['title']         = 'Login';
        // $data['contents']      = 'brand/list_brand';
        // List brand
        // $data['brands']        =  $this->brand_model->_get_all();
        $this->load->view('login/login', $data);
    }

}