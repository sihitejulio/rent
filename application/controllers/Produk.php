<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Produk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        $this->load->model('barang_model');
        $this->load->model('produk_model');
        // $this->load->model('produk_model');
        // $this->load->model('brand_model');
        // $this->load->model('kategori_model');
    }

    public function index(){
        $data['title']         = 'Eightquips Produk';
        $data['title_content'] = 'List Produk';
        $data['contents']      = 'produk/list_produk';
        // List Barang
        $data['produks']       =  $this->produk_model->_get_all();
        $this->load->view('main/app', $data);
    }

    public function add(){
        $this->session->unset_userdata('add_produk_barang');

        $data['title']         = 'Eightquips Produk';
        $data['title_content'] = 'Add Produk';
        $data['contents']      = 'produk/form_produk';
        $data['auto_produk_code']= $this->produk_model->_max_id();

        // $this->session->set_userdata('produk_add', array('produk_1'=>'test'));
        // $session_produk=$this->session->userdata('produk_add');
        // if(isset($session_produk)) { 
        //     //take them back to signin 
        //     $data['list_barang']=$this->session->userdata['produk_add'];
        // }
        $this->load->view('main/app', $data);
    }

    public function save(){
        $jsonresult = array();

        $this->form_validation->set_rules('kode_produk', 'Kode Produk', 'required');
        $this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('paket_tipe', 'Keterangan', 'required');
        $this->form_validation->set_rules('harga', 'Keterangan', 'required');
        $this->form_validation->set_rules('deskripsi', 'Keterangan', 'required');
      
        if ($this->form_validation->run() == false) {
			$jsonresult['status_valid'] = 'false';
            $jsonresult['err_kode_produk'] = form_error('kode_produk','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_nama_produk'] = form_error('nama_produk','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_paket_tipe']  = form_error('paket_tipe','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_harga']       = form_error('harga','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_deskripsi']   = form_error('deskripsi','<div class="help-block" style="color:red;">', '</div>');
        } else {
            $uuid         = $this->input->post('uuid_produk');
            $kode_produk  = $this->input->post('kode_produk');
            $nama_produk  = $this->input->post('nama_produk');
            $paket_tipe   = $this->input->post('paket_tipe');
            $harga        = $this->input->post('harga');
            $deskripsi    = $this->input->post('deskripsi');

            $message           = '';
                if(!isset($uuid)){
                    $data_produk=array(
                        'uuid_produk'  => $this->gen_uuid->generate_uuid(),
                        'kode_produk'  => $kode_produk,
                        'nama_produk'  => $nama_produk,
                        'package'      => $paket_tipe,
                        'harga'        => $harga,
                        'deskripsi'    => $deskripsi,
                        'created_at'   => date('Y-m-d H:i:s'),
                        'updated_at'   => date('Y-m-d H:i:s'),
                        'status'       => '1'// 1: aktif
                    );
                    // inser produk & detail
                    $listProdBarang = $this->session->userdata('add_produk_barang');
                    if(count($listProdBarang['list'])>0){

                        $add_produk=$this->produk_model->_insert($data_produk);
                        // After Insert
                        if($add_produk){
                            foreach ($listProdBarang['list'] as $key => $value) {
                                $data_detail_produk=array(
                                    'uuid_detail_produk' => $this->gen_uuid->generate_uuid(),
                                    'uuid_barang'        => $key,
                                    'uuid_produk'        => $data_produk['uuid_produk'],
                                    'created_at'         => date('Y-m-d H:i:s'),
                                    'updated_at'         => date('Y-m-d H:i:s')
                                );
                                $add_produk_detail=$this->produk_model->_insert_detail($data_detail_produk);
                            }
                            $message='Produk added successfully.';
                        }else{
                            $message='Produk gagal insert.';
                        }
                    }else{
                        $message='Produk gagal insert.';
                    }
                }else{
                    $data_produk['wheres']=array('uuid_produk'        => $uuid);
                    $data_produk['updates']=array(
                        'kode_produk'  => $kode_produk,
                        'nama_produk'  => $nama_produk,
                        'package'      => $paket_tipe,
                        'harga'        => $harga,
                        'deskripsi'    => $deskripsi,
                        'updated_at'   => date('Y-m-d H:i:s'),
                        'status'       => '1'// 1: aktif
                    );

                    // inser produk & detail
                    $listProdBarang = $this->session->userdata('add_produk_barang');

                    // delete Detail
                    $this->produk_model->_delete_detail_produk($uuid);
                    
                    if(count($listProdBarang['list'])>0){

                        $update=$this->produk_model->_update($data_produk);
                        // After Insert
                        if($update){
                            foreach ($listProdBarang['list'] as $key => $value) {
                                $data_detail_produk=array(
                                    'uuid_detail_produk' => $this->gen_uuid->generate_uuid(),
                                    'uuid_barang'        => $key,
                                    'uuid_produk'        => $uuid,
                                    'created_at'         => date('Y-m-d H:i:s'),
                                    'updated_at'         => date('Y-m-d H:i:s')
                                );
                                $add_produk_detail=$this->produk_model->_insert_detail($data_detail_produk);
                            }
                            $message='Produk update successfully.';
                        }else{
                            $message='Produk update insert.';
                        }
                    }else{
                        $message='Produk update insert.';
                    }
                }
                $jsonresult['msg']=$message;
            }
		echo json_encode($jsonresult);
    }

    public function edit(){
        $this->session->unset_userdata('add_produk_barang');


        $data['title']         = 'Eightquips Produk';
        $data['title_content'] = 'Edit Produk';
        $produk_id=$this->input->get('produk_id');
        // Check exists ID Barang
        if(isset($produk_id)){
            $data['contents']        = 'produk/form_produk';
            $data['produks']         =  $this->produk_model->_get_where(array('uuid_produk'=>$produk_id))->row_array();
            $data['barang_produk']   =  $this->produk_model->_get_barang_where(array('produk.uuid_produk'=>$produk_id));
           
            foreach ($data['barang_produk']->result_array() as $r_barang_produk) {
                $barang_id=$r_barang_produk['uuid_barang'];
                $data_session['list'][$barang_id]=1;
                $this->session->set_userdata('add_produk_barang',$data_session);
            }           
        }
        $this->load->view('main/app', $data);
    }

    // Barang Manage
    public function addProdukBarang(){
        $barangAdd      = $this->input->post('barang');
        $data           = array('list'=>array());
        // Add array
        $msg='0';
        
        $listProdBarang=$this->session->userdata('add_produk_barang');
        if(isset($listProdBarang)){
            $data   = $listProdBarang;
            if(!isset($data['list'][$barangAdd])){
                $data['list'][$barangAdd]=1;
                $this->session->set_userdata('add_produk_barang',$data);
                $msg='1';
            }else{
                $msg=   '0';
            }
        }else{
            // array_push($data['list'],$barangAdd);
            $data['list'][$barangAdd]=1;
            $this->session->set_userdata('add_produk_barang',$data);
            // echo '1';
            $msg=   '1';
        }
        echo $msg;
    }

    public function removeProdukBarang(){
        $id     = $this->input->post('trId');
        $listProdBarang = $this->session->userdata('add_produk_barang');
        unset($listProdBarang['list'][$id]);
        $this->session->set_userdata('add_produk_barang',$listProdBarang);
        print_r($listProdBarang);
    } 

    
    public function select_produk()
    {
        $json = [];
        $code_search = $this->input->get('q');
        $data = $this->produk_model->get_produk($code_search);
        foreach ($data as $row) {
            $json[] = ['id'=>$row->uuid_produk, 'text'=>$row->kode_produk."|".$row->nama_produk];
        }

        echo json_encode($json);
    }

  
    public function getBarangDetail(){
        $id     = $this->input->post('trId');
        echo json_encode($id);


    }
}