<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Invoice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        $this->load->model('invoice_model');
    }

    public function index(){
        $data['title']         = 'Eightquips Inv';
        $data['title_content'] = 'List Invoice';
        $data['contents']      = 'invoice/list_invoice';
        // List Barang
        $data['invoices']     =  $this->invoice_model->_get_all_join();
        $this->load->view('main/app', $data);
    }

    public function add(){
        // uri inv
        $data['title']         = 'Eightquips Barang';
        $data['title_content'] = 'Add Invoice';
        $data['contents']      = 'invoice/form_invoice';
        $data['inv_kode']      = $this->invoice_model->_max_id();

        // get uri segment
        $data['uuid_rental']  = $this->uri->segment('3');
        $data_inv             = $this->invoice_model->_get_where(array('uuid_rental'=>$data['uuid_rental'] ))->row_array();
        
        // $data['invoice']='';  
        if(!isset($data_inv['status_inv'])){
            $data['status_inv']      = 0;
        }else{
            $data['status_inv']      = $data_inv['status_inv'];
            $data['invoice']=$data_inv;  
        }

        $this->load->view('main/app', $data);
    }

    public function save(){
        $jsonresult = array();

        $this->form_validation->set_rules('tipe_bayar', 'Tipe Bayar', 'required');

      
        if ($this->form_validation->run() == false) {
			$jsonresult['status_valid_pilih_tipe']      = 'false';
            $jsonresult['err_tipe_bayar']    = form_error('tipe_bayar','<div class="help-block" style="color:red;">', '</div>');
          } else {
            // $jsonresult['status_valid'] = 'true';
            $tipe_bayar  = $this->input->post('tipe_bayar');
            $dp_bayar  = 0;
            $pelunasan = 0;

            if($tipe_bayar!=2){
                $this->form_validation->set_rules('dp_bayar', 'Dp Bayar', 'required');
                if ($this->form_validation->run() == false) {
                     $jsonresult['status_valid_dp'] = 'false';
                    
                }else{
                    $jsonresult['err_dp_bayar']    = form_error('dp_bayar','<div class="help-block" style="color:red;">', '</div>');
                    $dp_bayar  = $this->input->post('dp_bayar');
                    $kode_inv  = $this->input->post('kode_inv');
                    $uuid_rental  = $this->input->post('uuid_rental');
                    $keterangan  = $this->input->post('keterangan');
                    $data_inv=array(
                       'uuid_inv'       => $this->gen_uuid->generate_uuid(),
                       'uuid_rental'    => $uuid_rental,
                       'kode_inv'       => $kode_inv,
                       'dp_bayar'       => $dp_bayar,
                       'keterangan'     => $keterangan,
                       'created_at'     => date('Y-m-d H:i:s'),
                       'updated_at'     => date('Y-m-d H:i:s'),
                       'status_inv'         => '1'// 1: aktif
                   );
                   $add_inv=$this->invoice_model->_insert($data_inv);
                   // After Insert
                   if($add_inv){
                       $message='Invoice added successfully.';
                   }else{
                       $message='Invoice gagal insert.';
                   }
                   $jsonresult['msg']=$message;
                }
            }else{
                $this->form_validation->set_rules('pelunasan', 'Pelunasan', 'required');
                if ($this->form_validation->run() == false) {
                    $jsonresult['status_valid_pelunasan'] = 'false';
                }else{
                    $jsonresult['err_pelunasan']    = form_error('pelunasan','<div class="help-block" style="color:red;">', '</div>');
                    $pelunasan  = $this->input->post('pelunasan');         
                    $uuid  = $this->input->post('uuid_inv');       
                    
                    $data_inv             = $this->invoice_model->_get_where(array('uuid_inv'=>$uuid))->row_array();
                    
                    $data_inv['wheres']=array('uuid_inv' => $uuid);
                    $data_inv['updates']=array(
                        'total_bayar'   => $data_inv['dp_bayar']+$pelunasan,
                        'updated_at'    => date('Y-m-d H:i:s'),
                        'status_inv'    => '2'// 1: aktif
                    );
                    $add_inv=$this->invoice_model->_update($data_inv);
                    // After Insert
                    if($add_inv){
                        $message='Invoice added successfully.';
                    }else{
                        $message='Invoice gagal insert.';
                    }    
                    
                    $jsonresult['msg']=$message;
                }
            }
        }
		echo json_encode($jsonresult);
    }
}