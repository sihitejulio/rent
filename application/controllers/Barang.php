<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('gen_uuid');
        $this->load->model('barang_model');
        $this->load->model('brand_model');
        $this->load->model('kategori_model');
    }

    public function index(){
        $data['title']         = 'Eightquips Barang';
        $data['title_content'] = 'List Barang';
        $data['contents']      = 'barang/list_barang';
        // List Barang
        $data['barangs']     =  $this->barang_model->_get_all_join();
        $this->load->view('main/app', $data);
    }

    public function add(){
        $data['title']         = 'Eightquips Barang';
        $data['title_content'] = 'Add Barang';
        $data['contents']      = 'barang/form_barang';
        $data['brand']         =  $this->brand_model->_get_all();
        $data['kategori']      =  $this->kategori_model->_get_all();
        $data['auto_barang_code']= $this->barang_model->_max_id();

        $this->load->view('main/app', $data);
    }

    public function save(){
        $jsonresult = array();

        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');
        $this->form_validation->set_rules('brand', 'Brand', 'required');
        $this->form_validation->set_rules('detail_barang', 'Detail Barang', 'required');
        $this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required');

      
        if ($this->form_validation->run() == false) {
			$jsonresult['status_valid']      = 'false';
            $jsonresult['err_nama_barang']    = form_error('nama_barang','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_kode_barang']    = form_error('kode_barang','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_detail_barang'] = form_error('detail_barang','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_brand']         = form_error('brand','<div class="help-block" style="color:red;">', '</div>');
            $jsonresult['err_kategori']      = form_error('kategori','<div class="help-block" style="color:red;">', '</div>');
        } else {
            $uuid               = $this->input->post('uuid_barang');
            $kode_barang        = $this->input->post('kode_barang');
            $barang             = $this->input->post('nama_barang');
            $uuid_brand         = $this->input->post('brand');
            $uuid_kategori      = $this->input->post('kategori');
            $keterangan_barang  = $this->input->post('detail_barang');
            $message            = '';
                if(!isset($uuid)){
                            $data_barang=array(
                                'uuid_barang'   => $this->gen_uuid->generate_uuid(),
                                'uuid_brand'    => $uuid_brand,
                                'uuid_kategori' => $uuid_kategori,
                                'kode_barang'   => $kode_barang,
                                'nama_barang'   => $barang,
                                'info_detail'   => $keterangan_barang,
                                'created_at'    => date('Y-m-d H:i:s'),
                                'updated_at'    => date('Y-m-d H:i:s'),
                                'status'        => '1'// 1: aktif
                            );
                            $add_barang=$this->barang_model->_insert($data_barang);
                            // After Insert
                            if($add_barang){
                                $message='Barang added successfully.';
                            }else{
                                $message='Barang gagal insert.';
                            }
                }else{
                            $data_barang['wheres']=array('uuid_barang'        => $uuid);
                            $data_barang['updates']=array(
                                'uuid_brand'    => $uuid_brand,
                                'uuid_kategori' => $uuid_kategori,
                                'kode_barang'   => $kode_barang,
                                'nama_barang'  => $barang,
                                'info_detail'  => $keterangan_barang,
                                'updated_at'   =>  date('Y-m-d H:i:s')
                            );
                            $update_barang=$this->barang_model->_update($data_barang);
                            // After Insert
                            if($update_barang){
                                $message='Barang update successfully.';
                            }else{
                                $message='Barang gagal update.';
                            }
                }
                $jsonresult['msg']=$message;
            }
		echo json_encode($jsonresult);
    }

    public function edit(){
        $data['title']         = 'Eightquips Barang';
        $data['title_content'] = 'Edit Barang';
        $barang_id=$this->input->get('barang_id');
        // Check exists ID Barang
        if(isset($barang_id)){
            $data['contents']      = 'barang/form_barang';
            $data['brand']         =  $this->brand_model->_get_all();
            $data['kategori']      =  $this->kategori_model->_get_all();
            $data['barang']        =  $this->barang_model->_get_where(array('uuid_barang'=>$barang_id))->row_array();
        }

        $this->load->view('main/app', $data);
    }


    //--------------------------
    public function select_barang()
    {
        $json = [];
        $code_search = $this->input->get('q');
        $data = $this->barang_model->get_barang($code_search);
        foreach ($data as $row) {
            $json[] = ['id'=>$row->uuid_barang, 'text'=>$row->kode_barang."|".$row->nama_barang."|Brand : ".$row->nama_brand."|Kategori : ".$row->nama_kategori];
        }

        echo json_encode($json);
    }

}