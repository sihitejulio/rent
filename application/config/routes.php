<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/** Admin */
$route['admin']         = 'Admin/index';
$route['admin/add']     = 'Admin/add';
$route['admin/edit']    = 'Admin/edit';
$route['admin/update']  = 'Admin/update';
$route['admin/delete']  = 'Admin/delete';

/** Customer */
$route['customer']         = 'Customer/index';
$route['customer/add']     = 'Customer/add';
$route['customer/edit']    = 'Customer/edit';
$route['customer/update']  = 'Customer/update';
$route['customer/delete']  = 'Customer/delete';
/** Brand */
$route['brand']            = 'Brand/index';
$route['brand/add']        = 'Brand/add';
$route['brand/edit']       = 'Brand/edit';
$route['brand/update']     = 'Brand/update';
$route['brand/delete']     = 'Brand/delete';
/** Barang */
$route['barang']            = 'Barang/index';
$route['barang/add']        = 'Barang/add';
$route['barang/edit']       = 'Barang/edit';
$route['barang/update']     = 'Barang/update';
$route['barang/delete']     = 'Barang/delete';
/** Categorie */
$route['categorie']            = 'Categorie/index';
$route['categorie/add']        = 'Categorie/add';
$route['categorie/edit']       = 'Categorie/edit';
$route['categorie/update']     = 'Categorie/update';
$route['categorie/delete']     = 'Categorie/delete';
/** Produk */
$route['produk']            = 'Produk/index';
$route['produk/add']        = 'Produk/add';
$route['produk/edit']       = 'Produk/edit';
$route['produk/update']     = 'Produk/update';
$route['produk/delete']     = 'Produk/delete';

/** Produk */
$route['sewa']            = 'Sewa/index';
$route['sewa/add']        = 'Sewa/add';
$route['sewa/edit']       = 'Sewa/edit';
$route['sewa/update']     = 'Sewa/update';
$route['sewa/delete']     = 'Sewa/delete';
$route['sewa/addSewaProduk'] = 'Sewa/addSewaProduk';
$route['sewa/detail']        = 'Sewa/detail';

// add Produk Barang
$route['produk/addProdukBarang']     = 'Produk/addProdukBarang';
$route['produk/removeProdukBarang']  = 'Produk/removeProdukBarang';
//INV

/** Invoice */
$route['invoice']          = 'Invoice/index';
$route['invoice/add']      = 'Invoice/add';


$route['default_controller'] = 'Login/index';
// $route['default_controller'] = 'sewa';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
