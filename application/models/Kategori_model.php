<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('kategori');
    }
 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('kategori');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _insert($data)
    { 
        $this->db->insert('kategori', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('kategori', $data['updates']);
        return true;
    }

    // public function _count_rows(){

    // }


}