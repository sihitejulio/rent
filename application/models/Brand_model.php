<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('brands');
    }
 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('brands');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _insert($data)
    { 
        $this->db->insert('brands', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('brands',$data['updates']);
        return true;
    }
}