<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('invoice');
    }
 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('invoice');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _get_all_join(){
        $this->db->select('*,invoice.keterangan as inv_ket,invoice.created_at as inv_created_at,invoice.updated_at as inv_updated_at');
        $this->db->from('invoice');
        $this->db->join('rental', 'rental.uuid_rental = invoice.uuid_rental');
        $query = $this->db->get();
        return $query;
    }

    public function _insert($data)
    { 
        $this->db->insert('invoice', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('invoice', $data['updates']);
        return true;
    }

    
    public function _max_id(){
        $this->db->select('RIGHT(invoice.kode_inv,9) as kode', FALSE);
        $this->db->order_by('kode_inv','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('invoice');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //jika kode ternyata sudah ada.      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        }
        else {      
            //jika kode belum ada      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 9, "0", STR_PAD_LEFT);
        $kodejadi = "INV-".$kodemax;    
        return $kodejadi;  
    }

  
}