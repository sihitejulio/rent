<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('barang');
    }

    public function _get_all_join(){
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->join('brands', 'brands.uuid_brand = barang.uuid_brand');
        $this->db->join('kategori', 'kategori.uuid_kategori = barang.uuid_kategori');
        $query = $this->db->get();
        return $query;
    }
 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _insert($data)
    { 
        $this->db->insert('barang', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('barang',$data['updates']);
        return true;
    }

    public function get_barang($code){
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->join('brands', 'brands.uuid_brand = barang.uuid_brand');
        $this->db->join('kategori', 'kategori.uuid_kategori = barang.uuid_kategori');
        $this->db->like('nama_barang', $code, 'both');
        return $this->db->get()->result();
    }


    public function _max_id(){
        $this->db->select('RIGHT(barang.kode_barang,3) as kode', FALSE);
        $this->db->order_by('kode_barang','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('barang');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //jika kode ternyata sudah ada.      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        }
        else {      
            //jika kode belum ada      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodejadi = "BRG-".$kodemax;    
        return $kodejadi;  
    }
}