<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('customer');
    }

    // public function _get_all_join(){
    //     $this->db->select('*');
    //     $this->db->from('customer');
    //     $this->db->join('customer', 'brands.uuid_brand = barang.uuid_brand');
    //     $this->db->join('kategori', 'kategori.uuid_kategori = barang.uuid_kategori');
    //     $query = $this->db->get();
    //     return $query;
    // }

  

    public function _max_id(){
        $this->db->select('RIGHT(customer.kode_customer,4) as kode', FALSE);
        $this->db->order_by('kode_customer','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('customer');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //jika kode ternyata sudah ada.      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        }
        else {      
            //jika kode belum ada      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = "CUST-".$kodemax;    
        return $kodejadi;  
    }
 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _insert($data)
    { 
        $this->db->insert('customer', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('customer',$data['updates']);
        return true;
    }

    public function get_customer($code){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->or_like('kode_customer', $code);
        $this->db->or_like('nama_customer', $code);
        return $this->db->get()->result();
    }
}