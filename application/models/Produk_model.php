<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('produk');
    }
 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _get_detail_produk_where($data)
    { 
        $this->db->select('*');
        $this->db->from('detail_produk');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _get_barang_where($data)
    { 
        $this->db->select('*');
        $this->db->from('produk');
        //  $this->db->from('barang');
        $this->db->join('detail_produk', 'detail_produk.uuid_produk = produk.uuid_produk');
        $this->db->join('barang', 'barang.uuid_barang = detail_produk.uuid_barang');
        $this->db->join('brands', 'brands.uuid_brand = barang.uuid_brand');
        $this->db->join('kategori', 'kategori.uuid_kategori = barang.uuid_kategori');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _insert($data)
    { 
        $this->db->insert('produk', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _insert_detail($data)
    { 
        $this->db->insert('detail_produk', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('produk', $data['updates']);
        return true;
    }


    function _delete_detail_produk($id){
        $this->db->where('uuid_Produk', $id);
        $this->db->delete('detail_produk');
    }

    public function _max_id(){
        $this->db->select('RIGHT(produk.kode_produk,4) as kode', FALSE);
        $this->db->order_by('kode_produk','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('produk');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //jika kode ternyata sudah ada.      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        }
        else {      
            //jika kode belum ada      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodejadi = "PRD-".$kodemax;    
        return $kodejadi;  
    }

    public function get_produk($code){
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->or_like('kode_produk', $code);
        $this->db->or_like('nama_produk', $code);
        return $this->db->get()->result();
    }
}