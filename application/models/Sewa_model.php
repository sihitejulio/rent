<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewa_model extends CI_Model
{
    public function _get_all(){
        return $this->db->get('rental');
    }

    
    public function _get_all_join($data=''){
      $this->db->select('*,rental.status_rent As status_rent');
      $this->db->from('rental');
      $this->db->join('customer', 'customer.uuid_customer = rental.uuid_customer');
      if($data!=''){
        $this->db->where($data);
      }
      $query = $this->db->get();
      return $query;
    }

    public function _get_all_join_produk($data=''){
        $this->db->select('*');
        $this->db->from('detail_rental');
        $this->db->join('rental', 'rental.uuid_rental = detail_rental.uuid_rental');
        $this->db->join('produk', 'produk.uuid_produk = produk.uuid_produk');
        if($data!=''){
          $this->db->where($data);
        }
        $query = $this->db->get();
        return $query;
    }
  
    // public function _get_where_customer($data){
    //     $this->db->select('*');
    //     $this->db->from('customer');
    //     $this->db->join('customer', 'brands.uuid_brand = barang.uuid_brand');
    //     // $this->db->join('kategori', 'kategori.uuid_kategori = barang.uuid_kategori');
    //     $query = $this->db->get();
    //     return $query;
    // }


// $data['info_client']   =  $this->sewa_model->_get_where_customer(array('uuid_rental'=> $data['uuid_rental']))->row_array();


 
    public function _get_where($data)
    { 
        $this->db->select('*');
        $this->db->from('rental');
        $this->db->where($data);
        return $this->db->get();
    }

    public function _insert($data)
    { 
        $this->db->insert('rental', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function _update($data)
    { 
        $this->db->where($data['wheres']);
        $this->db->update('rental', $data['updates']);
        return true;
    }

    public function _insert_detail($data)
    { 
        $this->db->insert('detail_rental', $data);
        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
    }

    public function _max_id(){
        $this->db->select('RIGHT(rental.kode_rental,9) as kode', FALSE);
        $this->db->order_by('kode_rental','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('rental');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            //jika kode ternyata sudah ada.      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        }
        else {      
            //jika kode belum ada      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 9, "0", STR_PAD_LEFT);
        $kodejadi = "RENT-".$kodemax;    
        return $kodejadi;  
    }

    
    /** Produk Rental */
    public function _get_produk_where($data)
    { 
        $this->db->select('*');
        $this->db->from('detail_rental');
        //  $this->db->from('barang');
        $this->db->join('produk', 'produk.uuid_produk = detail_rental.uuid_produk');
        $this->db->where($data);
        return $this->db->get();
    }

    /** Detail Rental */
    public function _get_detail_where($data)
    { 
        $this->db->select('*');
        $this->db->from('detail_rental');
        //  $this->db->from('barang');
        $this->db->join('produk', 'produk.uuid_produk = detail_rental.uuid_produk');
        $this->db->join('detail_produk', 'detail_produk.uuid_produk = produk.uuid_produk');
        $this->db->join('barang', 'barang.uuid_barang = detail_produk.uuid_barang');
        $this->db->join('brands', 'brands.uuid_brand = barang.uuid_brand');
        $this->db->join('kategori', 'kategori.uuid_kategori = barang.uuid_kategori');
        $this->db->where($data);
        return $this->db->get();
    }

}