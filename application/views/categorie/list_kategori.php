<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('categorie/add'); ?>" class="btn btn-sm btn-outline-secondary">New</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Categorie Name </th>
            <th>Keterangan</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <?php 
                   $no=1;
                   foreach ($kategoris->result_array() as $r_kategori) {
            ?>  
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $r_kategori['nama_kategori']; ?></td>
                    <td><?php echo $r_kategori['keterangan']; ?></td>
                    <td><?php echo $r_kategori['created_at']; ?></td>
                    <td><?php echo $r_kategori['updated_at']; ?></td>
                    <td><a href="<?php echo base_url().'categorie/edit/?categorie_id='.$r_kategori['uuid_kategori'];?>" class="btn btn-sm btn-outline-secondary" >Edit</a></td>
                </tr>  
            <?php
                   }
            ?>
        </tbody>
        </table>
    </div>
</div>
