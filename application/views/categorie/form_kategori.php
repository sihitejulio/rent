<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>  
   
</div>
    <div class="row">
        <div class="col-md-6 order-md-1">
            <form id="form_kategori" method="post" action="<?php echo base_url();?>categorie/save">
                <div class="mb-3">
                    <?php if(isset($kategori)){
                        // Hide UUId Brand    
                    ?>
                        <input type="hidden" class="form-control" id="uuid_kategori" name="uuid_kategori" <?php echo 'value="'.$kategori['uuid_kategori'].'"'; ?>>
                    <?php }?>
                    <label for="nama_kategori">Nama<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" placeholder="Nama"  <?php if(isset($kategori)){ echo 'value="'.$kategori['nama_kategori'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="nama_kategori">Keterangan <span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="nama_kategori" name="keterangan" placeholder="Keterangan" <?php if(isset($kategori)){ echo 'value="'.$kategori['keterangan'].'"'; } ?>>
                </div>
            
                <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
                <hr class="mb-4">
            </form>
        </div>
    </div>
</div>
