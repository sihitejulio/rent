<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('customer/add'); ?>" class="btn btn-sm btn-outline-secondary">New</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode Customer</th>
                    <th>Nama Customer</th>
                    <th>No Hp</th>
                    <th>Email</th>
                    <th>No Telp</th>
                    <th>Tipe Customer</th>
                    <th>Alamat Tinggal</th>
                    <th>Alamat Kantor</th>
                    <th>Pasport Id</th>
                    <th>Npwp</th>
                    <th>Nama Perusahaan</th>
                    <th>Alamat Perusahaan</th>
                    <th>Jenis Industri</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no=1;
                    foreach ($customer->result_array() as $r_customer) {
                ?>  
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r_customer['kode_customer']; ?></td>
                        <td><?php echo $r_customer['nama_customer']; ?></td>
                        <td><?php echo $r_customer['no_hp']; ?></td>
                        <td><?php echo $r_customer['email']; ?></td>
                        <td><?php echo $r_customer['no_telp']; ?></td>
                        <td>
                            <?php  //'1 Perusahaan\r\n2 Pribadi\r\n3 Lain\r\n',
                                if($r_customer['tipe_customer']=='1'){
                                    echo 'Perusahaan'; 
                                }elseif($r_customer['tipe_customer']=='2'){
                                    echo 'Pribadi'; 
                                }else{
                                    echo 'Lain'; 
                                }
                            ?>
                        </td>
                        <td><?php echo $r_customer['alamat_tinggal']; ?></td>
                        <td><?php echo $r_customer['alamat_kantor']; ?></td>
                        <td><?php echo $r_customer['pasport_id']; ?></td>
                        <td><?php echo $r_customer['npwp']; ?></td>
                        <td><?php echo $r_customer['nama_perusahaan']; ?></td>
                        <td><?php echo $r_customer['alamat_perusahaan']; ?></td>
                        <td><?php echo $r_customer['jenis_industri_perusahaan']; ?></td>
                        <td><?php echo $r_customer['created_at']; ?></td>
                        <td><?php echo $r_customer['updated_at']; ?></td>
                        <td><a href="<?php echo base_url().'customer/edit/?customer_id='.$r_customer['uuid_customer'];?>" class="btn btn-sm btn-outline-secondary" >Edit</a></td>
                    </tr>  
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
