<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>  
</div>
    <div class="row">
        <div class="col-md-6 order-md-1">
            <form id="form_customer" method="post" action="<?php echo base_url();?>customer/save">

                <div class="mb-3">
                    <label for="tipe_customer">Tipe Customer<span class="text-muted"></span></label>
                    <select class="form-control" id="tipe_customer" name="tipe_customer">
                        <option value="">Pilih Tipe Customer</option>
                        <option value="1" <?php  if(isset($customer)&& $customer['tipe_customer']==1){ echo "selected='selected'"; } ?>>Perusahaan</option>
                        <option value="2" <?php  if(isset($customer)&& $customer['tipe_customer']==2){ echo "selected='selected'"; } ?>>Pribadi</option>
                        <option value="3" <?php  if(isset($customer)&& $customer['tipe_customer']==3){ echo "selected='selected'"; } ?>>Lain</option>
                    </select>
                </div>
                
                <hr class="mb-4">
                <div class="mb-3">
                    <label for="kode_customer" class="text-bold">Kode Customer</span></label>
                    <input type="text" class="form-control" id="kode_customer" name="kode_customer" placeholder="No Hp"  <?php if(isset($customer)){ echo 'value="'.$customer['kode_customer'].'"'; }else{ echo 'value="'.$auto_customer_code.'"'; } ?> readonly>
                </div>
                <div class="mb-3">
                    <?php if(isset($customer)){
                        // Hide UUId Customer    
                    ?>
                        <input type="hidden" class="form-control" id="uuid_customer" name="uuid_customer" <?php echo 'value="'.$customer['uuid_customer'].'"'; ?>>
                    <?php }?>
                    <label for="nama_customer">Nama Customer<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="nama_customer" name="nama_customer" placeholder="Nama Customer"  <?php if(isset($customer)){ echo 'value="'.$customer['nama_customer'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="no_hp">No Hp<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="No Hp"  <?php if(isset($customer)){ echo 'value="'.$customer['no_hp'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="no_telp">No Telp<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="No Telp"  <?php if(isset($customer)){ echo 'value="'.$customer['no_telp'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="email">Email<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email"  <?php if(isset($customer)){ echo 'value="'.$customer['email'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="pasport_id">Pasport Id<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="pasport_id" name="pasport_id" placeholder="Pasport id"  <?php if(isset($customer)){ echo 'value="'.$customer['pasport_id'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="npwp">Npwp<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="npwp" name="npwp" placeholder="Npwp"  <?php if(isset($customer)){ echo 'value="'.$customer['npwp'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="alamat_tinggal">Alamat Tinggal <span class="text-muted"></span></label>
                    <textarea class="form-control" id="alamat_tinggal" name="alamat_tinggal"> <?php if(isset($customer)){ echo $customer['alamat_tinggal']; }?> </textarea>
                </div>

                <div class="mb-3">
                    <label for="alamat_kantor">Alamat Kantor <span class="text-muted"></span></label>
                    <textarea class="form-control" id="alamat_kantor" name="alamat_kantor"> <?php if(isset($customer)){ echo $customer['alamat_kantor']; }?> </textarea>
                </div>

                <div class="mb-3">
                    <label for="nama_perusahaan">Nama Perusahaan<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan"  <?php if(isset($customer)){ echo 'value="'.$customer['nama_perusahaan'].'"'; } ?>>
                </div>
                
                <div class="mb-3">
                    <label for="npwp">Jenis Industri Perusahaan<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="jenis_perusahaan" name="jenis_perusahaan" placeholder=" "  <?php if(isset($customer)){ echo 'value="'.$customer['jenis_industri_perusahaan'].'"'; } ?>>
                </div>
                
                <div class="mb-3">
                    <label for="alamat_perusahaan">Alamat Perusahaan <span class="text-muted"></span></label>
                    <textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan"><?php if(isset($customer)){ echo $customer['alamat_perusahaan']; }?> </textarea>
                </div>
            
                <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
                <hr class="mb-4">
        </div>
    </div>
</div>
