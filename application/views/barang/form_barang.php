<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>  
</div>
    <div class="row">
        <div class="col-md-6 order-md-1">
            <form id="form_barang" method="post" action="<?php echo base_url();?>barang/save">
                <div class="mb-3">
                    <label for="kode_barang" class="text-bold">Kode Barang</span></label>
                    <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang"  <?php if(isset($barang)){ echo 'value="'.$barang['kode_barang'].'"'; }else{ echo 'value="'.$auto_barang_code.'"'; } ?> readonly>
                </div>
                <div class="mb-3">
                    <?php if(isset($barang)){
                        // Hide UUId barang    
                    ?>
                        <input type="hidden" class="form-control" id="uuid_barang" name="uuid_barang" <?php echo 'value="'.$barang['uuid_barang'].'"'; ?>>
                    <?php }?>
                    <label for="nama_barang">Nama<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="Nama"  <?php if(isset($barang)){ echo 'value="'.$barang['nama_barang'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="kategori">Kategori<span class="text-muted"></span></label>
                    <select class="form-control" id="kategori" name="kategori">
                        <option value="">Pilih Kategori</option>
                        <?php 
                            foreach ($kategori->result_array() as $r_kategori) {
                                if(isset($barang)){ 
                                    if($barang['uuid_kategori']==$r_kategori['uuid_kategori']){
                        ?>
                                        <option value="<?php echo $r_kategori['uuid_kategori']; ?>" selected='selected'><?php echo $r_kategori['nama_kategori']; ?></option>
                        <?php
                                    }else{
                        ?>
                                        <option value="<?php echo $r_kategori['uuid_kategori']; ?>"><?php echo $r_kategori['nama_kategori']; ?></option>
                        <?php
                                    }
                                }else{
                        ?>
                                     <option value="<?php echo $r_kategori['uuid_kategori']; ?>"><?php echo $r_kategori['nama_kategori']; ?></option>
                        <?php 
                                } 
                            }
                        ?>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="brand">Merk<span class="text-muted"></span></label>
                    <select class="form-control" id="brand" name="brand">
                    <option value="">Pilih Brand</option>
                        <?php 
                            foreach ($brand->result_array() as $r_brand) {
                                if(isset($barang)){ 
                        // Hide UUId barang    
                                    if($barang['uuid_brand']==$r_brand['uuid_brand']){
                        ?>
                                        <option value="<?php echo $r_brand['uuid_brand']; ?>" selected='selected'><?php echo $r_brand['nama_brand']; ?></option>
                        <?php
                                    }else{
                        ?>
                                        <option value="<?php echo $r_brand['uuid_brand']; ?>"><?php echo $r_brand['nama_brand']; ?></option>
                        <?php
                                    }
                                }else{
                        ?>
                                     <option value="<?php echo $r_brand['uuid_brand']; ?>"><?php echo $r_brand['nama_brand']; ?></option>
                        <?php 
                                } 
                            }
                        ?>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="detail_barang">Detail Barang <span class="text-muted"></span></label>
                    <textarea class="form-control" id="detail_barang" name="detail_barang"> <?php if(isset($barang)){ echo $barang['info_detail']; }?> </textarea>
                </div>
            
                <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
                <hr class="mb-4">
            </form>
        </div>
    </div>
</div>
