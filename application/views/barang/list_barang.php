<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('barang/add'); ?>" class="btn btn-sm btn-outline-secondary">New</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Brand</th>
            <th>Kategori</th>
            <th>Details</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <?php 
                  $no=1;
                  foreach($barangs->result_array() as $r_barang) {
            ?>  
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $r_barang['kode_barang']; ?></td>
                    <td><?php echo $r_barang['nama_barang']; ?></td>
                    <td><?php echo $r_barang['nama_brand']; ?></td>
                    <td><?php echo $r_barang['nama_kategori']; ?></td>
                    <td><?php echo $r_barang['info_detail']; ?></td>
                    <td><?php echo $r_barang['created_at']; ?></td>
                    <td><?php echo $r_barang['updated_at']; ?></td>
                    <td><a href="<?php echo base_url().'barang/edit/?barang_id='.$r_barang['uuid_barang'];?>" class="btn btn-sm btn-outline-secondary" >Edit</a></td>
                </tr>  
            <?php
                  }
            ?>
        </tbody>
        </table>
    </div>
</div>
