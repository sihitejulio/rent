<nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              
              <li class="nav-item">
                <a class="nav-link active" href="admin-dashboard-main.php">
                  <span data-feather="home"></span>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="admin-dashboard-side-admin.php" data-toggle="collapse">
                  <span data-feather="user"></span>
                  side admin account
                </a> -->
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('sewa');?>">
                  <span data-feather="file"></span>
                  Form Sewa
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="collapse">
                  <span data-feather="file"></span>
                  Orders
                </a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link" href="admin-dashboard-check-in.php">
                  <span data-feather="file"></span>
                  Check In
                </a>
              </li> -->
              
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('invoice');?>">
                  <span data-feather="file"></span>
                  Invoice
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('pengembalian');?>">
                  <span data-feather="file"></span>
                  Pengembalian
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="admin-dashboard-surat-jalan.php" >
                  <span data-feather="file"></span>
                  Surat Jalan
                </a>
              </li> -->
              <!-- <li class="nav-item">
                <a class="nav-link" href="admin-dashboard-quotation.php">
                  <span data-feather="file"></span>
                  Quotation
                </a>
              </li> -->
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('produk');?>">
                  <span data-feather="shopping-cart"></span>
                  Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link"  href="<?php echo base_url('barang');?>">
                  <span data-feather="file"></span>
                  Barang
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('categorie');?>">
                  <span data-feather="file"></span>
                  Categori
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('brand');?>">
                  <span data-feather="file"></span>
                  Brand
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('customer');?>">
                  <span data-feather="user"></span>
                  Customers
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('admin');?>">
                  <span data-feather="users"></span>
                  Admin
                </a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="../app/index.html">
                  <span data-feather="file"></span>
                  Front-End
                </a>
              </li> -->
            </ul>
          </div>
        </nav>
