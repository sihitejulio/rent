<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../admin/img/logos.ico">
    <title>Dashboard Main</title>
    <!-- Bootstrap core CSS -->
    <?php $this->load->view('main/_css'); //Loading Top Main Menu Navigation ?>
  </head>

  <body>
  <!-- Navbar -->
  <?php $this->load->view('main/nav_bar'); //Loading Top Main Menu Navigation ?>
    <div class="container-fluid">
      <div class="row">
      <!-- Side Bar -->
        <?php $this->load->view('main/sidebar'); //Loading Top Main Menu Navigation ?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <!-- Content -->
             <?php $this->load->view($contents); ?>
        </main>
      </div>
    </div>
    <!-- Script -->
    <?php $this->load->view('main/_js'); //Loading Top Main Menu Navigation ?>
  </body>
</html>
