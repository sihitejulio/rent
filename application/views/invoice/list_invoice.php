<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('produk/add'); ?>" class="btn btn-sm btn-outline-secondary">Rental Page</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode INV</th>
                    <th>Kode Rental</th>
                    <th>Keterangan</th>
                    <th>Dp Bayar</th>
                    <th>Pelunasan</th>
                    <th>Total</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Status</th>
                    <th>Action</th>
            </thead>
            <tbody>
           <?php 
                 $no=1;
                 foreach ($invoices->result_array() as $r_inv) {
            ?>  
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $r_inv['kode_inv']; ?></td>
                    <td><?php echo $r_inv['kode_rental']; ?></td>
                    <td><?php echo $r_inv['inv_ket'];?></td>
                    <td><?php echo $r_inv['dp_bayar']; ?></td>
                    <td><?php echo $r_inv['total_bayar']-$r_inv['dp_bayar']; ?></td>
                    <td><?php echo $r_inv['total_bayar']; ?></td>
                    <td><?php echo $r_inv['inv_created_at']; ?></td>
                    <td><?php echo $r_inv['inv_updated_at']; ?></td>
                    <td>
                        <?php 
                            switch ($r_inv['status_inv']) {
                                case '1':                            
                                    echo "<label class='badge badge-info'>Bayar Dp</label>"; 
                                    break;
                                case '2':                            
                                    echo "<label class='badge badge-info'>Lunas</label>"; 
                                    break;
                                
                                default:
                                    # code...
                                    break;
                            }
                        ?>
                    </td>
                    <td>
                        <?php 
                            switch ($r_inv['status_inv']) {
                                case '1':                            
                                    // echo "<label class='badge badge-info'>Bayar Dp</label>"; 
                                    echo '<a href="" class="btn btn-sm btn-success" >Pelunasan Invoice</a>'; 
                                    break;
                                
                                default:
                                    # code...
                                    break;
                            }
                        ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
