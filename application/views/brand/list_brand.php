<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('brand/add'); ?>" class="btn btn-sm btn-outline-secondary">New</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Details</th>
            <th>Create Date</th>
            <th>Update Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <?php 
                   $no=1;
                   foreach ($brands->result_array() as $r_brand) {
            ?>  
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $r_brand['nama_brand']; ?></td>
                    <td><?php echo $r_brand['keterangan']; ?></td>
                    <td><?php echo $r_brand['created_at']; ?></td>
                    <td><?php echo $r_brand['updated_at']; ?></td>
                    <td><a href="<?php echo base_url().'brand/edit/?brand_id='.$r_brand['uuid_brand'];?>" class="btn btn-sm btn-outline-secondary" >Edit</a></td>
                </tr>  
            <?php
                   }
            ?>
        </tbody>
        </table>
    </div>
</div>
