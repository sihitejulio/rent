<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>  
   
</div>
    <div class="row">
        <div class="col-md-6 order-md-1">
            <form id="form_brand" method="post" action="<?php echo base_url();?>brand/save">
                <div class="mb-3">
                    <?php if(isset($brand)){
                        // Hide UUId Brand    
                    ?>
                        <input type="hidden" class="form-control" id="uuid_brand" name="uuid_brand" <?php echo 'value="'.$brand['uuid_brand'].'"'; ?>>
                    <?php }?>
                    <label for="address2">Nama<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="nama_brand" name="nama_brand" placeholder="Nama"  <?php if(isset($brand)){ echo 'value="'.$brand['nama_brand'].'"'; } ?>>
                </div>

                <div class="mb-3">
                    <label for="address2">Keterangan <span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" <?php if(isset($brand)){ echo 'value="'.$brand['keterangan'].'"'; } ?>>
                </div>
            
                <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
                <hr class="mb-4">
            </form>
        </div>
    </div>
</div>
