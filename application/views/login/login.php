<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../admin/img/logos.ico">
    <title>Dashboard Main</title>
    <!-- Bootstrap core CSS -->
    <?php $this->load->view('main/_css'); ?>
</head>

<body class="text-center" data-gr-c-s-loaded="true">

    <form class="form-signin">
        <img class="mb-4" src="<?php echo base_url() ?>assets/img/logos_login.png " alt="" width="172" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">Username/Id</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Username/Id" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox mb-3">
            <label>
                <!-- <input type="checkbox" value="remember-me"> Remember me -->
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
    </form>

    <!-- Script -->
    <?php $this->load->view('main/_js'); ?>
</body>

</html> 