<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('sewa/add'); ?>" class="btn btn-sm btn-outline-secondary">New</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode Rental/Sewa</th>
                    <th>Kode Customer</th>
                    <th>Nama Customer</th>
                    <th>Status</th>
                    <th>Tanggal Mulai Sewa</th>
                    <th>Tanggal Berakhir Sewa</th>
                    <th>Create Date</th>
                    <th>Update Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                   $no=1;
                   foreach ($list_sewa->result_array() as $r_sewa) {
            ?>  
                <tr id="list-sewa-<?php echo $r_sewa['uuid_rental']; ?>">
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $r_sewa['kode_rental']; ?></td>
                    <td><?php echo $r_sewa['kode_customer']; ?></td>
                    <td><?php echo $r_sewa['nama_customer']; ?></td>
                    <td>
                        <?php 
                            switch ($r_sewa['status_rent']) {
                                case '1':                            
                                    echo "<label class='badge badge-info'>Pending</label>"; 
                                    break;
                                case '2':                            
                                    echo "<label class='badge badge-info'>Invoice , Down Payment</label>"; 
                                    break;
                                
                                default:
                                    # code...
                                    break;
                            }
                        ?>
                    </td>
                    <td><?php echo $r_sewa['tgl_mulai']; ?></td>
                    <td><?php echo $r_sewa['tgl_selesai']; ?></td>
                    <td><?php echo $r_sewa['created_at']; ?></td>
                    <td><?php echo $r_sewa['updated_at']; ?></td>
                    <td>
                        <?php 
                            switch ($r_sewa['status_rent']) {
                                case '1':          
                                    echo '<a href="'.base_url().'invoice/add/'.$r_sewa['uuid_rental'].'" class="btn btn-sm btn-success" >Buat Invoice</a>'; 
                                    echo '<a href="" class="btn btn-sm btn-danger" >Batal</a>'; 
                                    break;
                                case '2':
                                    echo '<a href="'.base_url().'suratjalan/add/'.$r_sewa['uuid_rental'].'" class="btn btn-sm btn-success" >Buat Surat Jalan</a>'; 
                                    echo '<a href="" class="btn btn-sm btn-success" >Lunas Invoice</a>'; 
                                    break;
                                case '3':           
                                    echo '<a href="'.base_url().'suratjalan/add/'.$r_sewa['uuid_rental'].'" class="btn btn-sm btn-success" >Buat Surat Jalan</a>'; 
                                    echo '<a href="" class="btn btn-sm btn-success" >Pengembalian</a>'; 
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                        ?>
                        <a target="_blank" rel="noopener noreferrer" href="<?php echo base_url().'sewa/detail'.'/'. $r_sewa['uuid_rental']; ?>" id="<?php echo $r_sewa['uuid_rental']; ?>" href="" class="btn btn-sm btn-outline-secondary detail-sewa" >Detail</a>
                    </td>
                </tr>  
                   <?php } ?>
            </tbody>
        </table>
    </div>
</div>
