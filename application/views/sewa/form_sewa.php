<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Add Form Sewa</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">New</button>
                <button class="btn btn-sm btn-outline-secondary">Edit</button>
                <button class="btn btn-sm btn-outline-secondary">Delete</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
            </div>
            <form id="form_sewa" method="post" action="<?php echo base_url();?>sewa/save">
            <div class="row">
                 <div class="mb-3 ml-3">
                    <label for="kode_customer" class="text-bold">Kode Rental</span></label>
                    <input type="text" class="form-control" id="kode_rental" name="kode_rental" placeholder="Kode Rntal" <?php if(isset($rental)){ echo 'value="'.$rental['auto_rental_code'].'"'; } else{ echo 'value="'.$auto_rental_code.'"'; } ?> readonly>
                </div>
          
              <div class="col-md-12 order-md-1">
                    <!-- <select class="js-example-basic-single form-control col-md-12 " id="barang_add" name="barang_add">
                    </select> -->
                  <h4 class="mb-3">Informasi Penyewa</h4>
                  <hr class="mb-4">
                  <label for="firstName" class="text-info">Cari Customer</label>

                  <div class="row">

                    <!-- <label for="firstName" class="text-info">Cari</label> -->
                    <div class="col-md-4 pt-1">
                      <select class="js-example-basic-single2 form-control col-md-12 " id="customer_id_add" name="customer_id" >
                      </select>
                    </div>
                    
                    <div class="col-md-2 pl-0 pt-1 mb-2">
                      <div class="btn-toolbar">
                        <div class="btn-group mr-2">
                        <!-- href="<?php //echo base_url('produk/add'); ?>" -->
                            <span  class="btn btn-sm btn-outline-secondary addCust">Add</span>
                        </div>
                      </div>
                    </div>
                  </div>

                      
                      

                  <hr class="mb-1">
                  <div class="row">
                    <div class="col-md-6 mb-1">
                      <label for="firstName">Kode Customer</label>
                      <div class="font-weight-bold" id="kode_customer"></div>
                    </div>
                    <div class="col-md-6 mb-1">
                      <label for="lastName">Type Customer</label>
                      <div class="font-weight-bold" id="type_customer"></div>
                    </div>
                  </div>
                  <hr class="mb-1">

                  <div class="row">
                    <div class="col-md-6 mb-1">
                      <label for="firstName">Nama Customer</label>
                      <div class="font-weight-bold" id="nama_customer"></div>
                      
                    </div>
                    <div class="col-md-6 mb-1">
                      <label for="lastName">Perusahaan</label>
                      <!-- <input type="text" class="form-control" id="perusahaan" placeholder="" value="" required> -->
                      <!-- <div class="invalid-feedback">
                        Perusahaan perlu di isi.
                      </div> -->
                    </div>
                  </div>

                  <hr class="mb-1">

                  <div class="row">
                    <div class="col-md-6 mb-1">
                      <label for="firstName">Handphone</label>
                      <div class="font-weight-bold" id="no_hp"></div>
                    </div>
                    <div class="col-md-6 mb-1">
                      <label for="lastName">Telp Kantor</label>
                      <div class="font-weight-bold" id="no_telp"></div>
                    </div>
                  </div>

                  <hr class="mb-1">

                  <div class="mb-1">
                    <label for="email">Email <span class="text-muted"></span></label>
                    <div class="font-weight-bold" id="email"></div>
                  </div>

                  <hr class="mb-4">

                  <div class="mb-3">
                    <label for="address">Alamat Tinggal</label>
                    <div class="font-weight-bold" id="alamat"></div>

                  </div>

              

                  <hr class="mb-1">

                  <div class="mb-3">
                    <label for="address2">Alamat Kantor <span class="text-muted"></span></label>
                    <div class="font-weight-bold" id="alamat_kantor"></div>
                  </div>

                 
                  <hr class="mb-4">

                  <div class="mb-3">
                    <label for="address2">Passport ID <span class="text-muted"></span></label>
                    <div class="font-weight-bold" id="passport"></div>

                  </div>

                  <div class="mb-3">
                    <label for="address2">NPWP <span class="text-muted"></span></label>
                    <div class="font-weight-bold" id="npwp"></div>
                  </div>

                  <div class="mb-3">
                    <label for="address2">Nama & Alamat Perusahaan Induk (Jika Anak Perusahaan)<span class="text-muted"></span></label>
                    <div class="font-weight-bold" id="alamat_perusahaan"></div>
                  
                  </div>

                  <div class="mb-3">
                    <label for="address2">Jenis Industri Perusaaan <span class="text-muted"></span></label>
                    <div class="font-weight-bold" id="jenis_industri_perusahaan"></div>
                  </div>


              </div>
            </div>
            <div class="row">
              <div class="col-md-12 order-md-1">
                  <h4 class="mb-3">Informasi Rental</h4>
                  <hr class="mb-4">
                  
                  <div class="mb-3">
                    <label for="address2">Project :<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="project_name" name="project_name" placeholder="Project">
                  </div>

                  <hr class="mb-4">
                  <div class="row">
                    <div class="col-md-5 pt-1">
                      <select class="js-example-basic-single3 form-control col-md-12 " id="prod_add" name="prod_add" >
                      </select>
                    </div>
                    
                    <div class="col-md-2 pl-0 pt-1 mb-2">
                      <div class="btn-toolbar">
                        <div class="btn-group mr-2">
                        <!-- href="<?php //echo base_url('produk/add'); ?>" -->
                            <span  class="btn btn-sm btn-outline-secondary addProduk">Add</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <table class="table table-striped table-sm produk_list_sewa">
                        <!-- <thead> -->
                            <tr>
                                <th>Kode Produk</th>
                                <th>Nama Produk</th>
                                <th>Deskripsi</th>
                                <th>Paket Tipe</th>
                                <th>Harga</th>
                                <th  style="width:30px;">Jumlah</th>
                                <th></th>
                            </tr>
                        <!-- <thead>
                        <tbody> -->
                            <?php 
                            if(isset($barang_produk)){

                                $no=1;
                                foreach ($barang_produk->result_array() as $r_barang_produk) {
                            ?>  
                                <tr id="<?php echo $r_barang_produk['uuid_barang']; ?>">
                                    <td><?php echo $r_barang_produk['kode_barang']; ?></td>
                                    <td><?php echo $r_barang_produk['nama_barang']; ?></td>
                                    <td><?php echo $r_barang_produk['nama_brand']; ?></td>
                                    <td><?php echo $r_barang_produk['nama_kategori']; ?></td>
                                    <td><a class="deleteBtnProduct">X</a></td>
                                </tr>  
                            <?php
                                    }
                                }
                            ?>
                        <!-- </tbody> -->
                    </table>
                </div>
<!--                   
                  <div class="mb-3">
                    <label for="address2">Jangka Waktu Rental :<span class="text-muted"></span></label>
                    <input type="text" class="form-control" id="address2" placeholder="Jangka Waktu Rental" required>
                  </div>
                   -->
                      <div class="mb-3 col-md-4">
                        <label for="address2">Tanggal Mulai Rental :<span class="text-muted"></span></label>
                        <input type="text" class="form-control" id="tanggal_mulai" name="tanggal_mulai" placeholder="Tanggal Mulai Rental">
                      </div>
                      
                      <div class="mb-3 col-md-4">
                        <label for="address2">Tanggal Berakhir Rental <span class="text-muted"></span></label>
                        <input type="text" class="form-control" id="tanggal_selesai" id="tanggal_selesai" placeholder="Tanggal Berakhir Rental">
                      </div>
                      
                      <div class="mb-3 col-md-4">
                        <label for="address2">Keterangan <span class="text-muted"></span></label>
                        <textarea class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan"></textarea>
                      </div>
              </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
            <hr class="mb-4">
          </div>
          </form>
