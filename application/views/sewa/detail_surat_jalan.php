<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../admin/img/logos.ico">
    <title>Surat Jalan</title>
    <!-- Bootstrap core CSS -->
    <?php $this->load->view('main/_css'); //Loading Top Main Menu Navigation ?>
  </head>

  <body>
    <div class="container">
    <div class="row">
        <div class="col-md-12 mt-4">
             <button class="btn btn-default hidden-print" onclick="myFunction()"><span data-feather="printer"></span> Print Surat</button>
        </div>
    </div>
        <div class="row">
            <div class="col-12">
                <div class="card  mt-5">
                    <div class="card-body p-0">
                        <div class="row p-5">
                            <div class="col-md-6">
                            <!-- <img src="../admin/img/logos1.png" width="400" height="100%"> -->
                            </div>

                            <div class="col-md-6 text-right">
                                <p class="font-weight-bold mb-1">Quotation #1</p>
                                <p class="text-muted">Tertanggal pada: 1 Oktober, 2018</p>
                                <p class="mb-1">jalan.kemang timur no.34</p>
                                <p class="mb-1">jakarta selatan [12510]</p>
                                <p class="mb-1">indonesia</p>
                                <p class="mb-1">M +62 877 0110 2828</p>
                                <p class="mb-1">M +62 812 1111 8282</p>
                                <p class="mb-1">E info@eightquips.com</p>
                                <p class="mb-1">W www.eightquips.com</p>
                            </div>
                        </div>

                        <hr class="my-5">

                        <div class="row pb-5 p-5">
                        <?php
                        // $id=$_GET['id_pesanan'];
                        // $form=mysql_fetch_assoc(getPesanan($id));
                    ?>
                            <div class="col-md-6">
                                <p class="font-weight-bold mb-4">CLIENT</p>
                                <p class="mb-1"><span class="text-muted">CLINET CODE : </span><?php echo $info_client['kode_customer'];?></p>
                                <p class="mb-1"><span class="text-muted">NAME : </span> <?php echo $info_client['nama_customer'];?></p>
                                <p class="mb-1"><span class="text-muted">TELP / PHONE NO: </span> <?php echo $info_client['no_telp'];?>/<?php echo $info_client['no_hp'];?></p>
                                <p class="mb-1"><span class="text-muted">EMAIL : </span> <?php echo $info_client['email'];?></p>
                                <p class="mb-1"><span class="text-muted">ALAMAT : </span><?php echo $info_client['alamat_tinggal'];?></p>
                                <p class="mb-1"><span class="text-muted">ALAMAT KANTOR/ PERUSAHAAN  : </span><?php echo $info_client['alamat_kantor'].'/ '.$info_client['alamat_perusahaan'];?></p>
                            </div>

                            <div class="col-md-6 text-right">
                                <p class="font-weight-bold mb-4">INFORMASI RENTAL</p>
                                <p class="mb-1"><span class="text-muted">PROJECT NO : </span><?php echo $info_client['kode_rental'];?></p>
                                <p class="mb-1"><span class="text-muted">PROJECT TITLE : </span> <?php echo $info_client['nama_project'];?></p>
                            </div>
                        </div>

                        <div class="row p-5">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="border-0 text-uppercase small font-weight-bold">PRODUCT CODE</th>
                                            <th class="border-0 text-uppercase small font-weight-bold">PRODUCT</th>
                                            <th class="border-0 text-uppercase small font-weight-bold">DAY</th>
                                            <th class="border-0 text-uppercase small font-weight-bold">TANGGAL PEMINJAMAN</th>
                                            <th class="border-0 text-uppercase small font-weight-bold">TANGGAL PENGEMBALIAN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach ($produk_detail->result_array() as $r_produk_detail) {
                                        ?>
                                        <tr>
                                            <td><?php echo $r_produk_detail['kode_produk']; ?></td>
                                            <td><?php echo $r_produk_detail['nama_produk']; ?></td>
                                            <td>
                                                <?php
                                                //  [tgl_mulai] => 2019-02-14 [tgl_selesai] 
                                                    $date1=date_create($r_produk_detail['tgl_mulai']);
                                                    $date2=date_create($r_produk_detail['tgl_selesai']);
                                                    $diff=date_diff($date1,$date2);
                                                    echo $diff->format('%d');
                                                ?>
                                            </td>
                                            <td><?php echo $r_produk_detail['tgl_mulai'];?></td>
                                            <td><?php echo $r_produk_detail['tgl_selesai'];?></td>
                                        </tr>
                                    <?php } ?>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="text-light mt-5 mb-5 text-center small"> <a class="text-light" target="_blank" href="EIGHTQUIPS">EIGHTQUIPS </a></div>
    </div>
    <?php $this->load->view('main/_js'); //Loading Top Main Menu Navigation ?>
  </body>
</html>
