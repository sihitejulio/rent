<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>  
   
</div>
    <div class="row">
        <div class="col-md-12 order-md-1">
            <form id="form_inv" method="post" action="<?php echo base_url();?>invoice/save">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="col-md-12 mb-3">
                        <input type="hidden" class="form-control" id="uuid_rental" name="uuid_rental">

                            <?php 
                                if($status_inv!=2){
                            ?>
                                <label for="tipe_bayar">Tipe Bayar<span class="text-muted"></span></label>
                                <select id="tipe_bayar" name="tipe_bayar" class="form-control">
                                    <option value="">Pilih Tipe</option>
                                    <?php
                                        if($status_inv!=1){
                                    ?>
                                        <Label>DP : RP<?php echo ?> </label>
                                        <option value="2">Lunas</option>
                                    <?php } else{ ?>
                                        <option value="1">Down Payment</option>
                                        <option value="2">Lunas</option>
                                    <?php }?>
                                </select>
                            <?php
                                }else{
                            ?>
                                <label for="kategori">Status INV Lunas</label>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="col-md-12 mb-3">
                            <label for="harga">Dp <span class="text-muted"></span></label>
                            <input type="text" class="form-control" id="dp_bayar" name="dp_bayar" placeholder="Dp Bayar" <?php if(isset($invoice)){ echo 'value="'.$invoice['dp_bayar'].'"'; } ?>>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="harga">Pelunasan <span class="text-muted"></span></label>
                            <input type="text" class="form-control" id="pelunasan" name="pelunasan" placeholder="Pelunasan" <?php if(isset($invoice)){ echo 'value="'.$invoice['total_bayar']-$invoice['dp_bayar'].'"'; } ?>>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="harga">TOTAL BAYAR <span class="text-muted"></span></label>
                            <input type="text" class="form-control" id="total" name="total" placeholder="TOTAL BAYAR" <?php if(isset($invoice)){ echo 'value="'.$invoice['total_bayar'].'"'; } ?> disabled>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="deskripsi">Deskripsi <span class="text-muted"></span></label>
                            <textarea class="form-control" id="keterangan" name="keterangan"> <?php if(isset($invoice)){ echo $invoice['keterangan']; }?> </textarea>
                        </div>
                    </div>
                </div>
                <hr class="col-md-12 mb-4">
                    <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
                <hr class="mb-12">
            </form>
        </div>
    </div>
</div>
