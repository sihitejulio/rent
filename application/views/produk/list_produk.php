<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?php echo base_url('produk/add'); ?>" class="btn btn-sm btn-outline-secondary">New</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive mr-3 ml-3">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kode Produk</th>
                    <th>Nama Produk</th>
                    <th>Package</th>
                    <th>Harga</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Status</th>
                    <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $no=1;
                    foreach ($produks->result_array() as $r_produks) {
                ?>  
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r_produks['kode_produk']; ?></td>
                        <td><?php echo $r_produks['nama_produk']; ?></td>
                        <td><?php if($r_produks['package']=='1'){echo 'Set'; }else{echo 'Item'; } ?></td>
                        <td><?php echo 'Rp'. number_format($r_produks['harga'],0,',','.'); ?></td>
                        <td><?php echo $r_produks['created_at']; ?></td>
                        <td><?php echo $r_produks['updated_at']; ?></td>
                        <td><?php if($r_produks['status']=='1'){echo 'Aktif'; }else{echo 'Tidak Aktif'; } ?></td>
                        <td><a href="<?php echo base_url().'produk/edit/?produk_id='.$r_produks['uuid_produk'];?>" class="btn btn-sm btn-outline-secondary" >Edit</a></td>
                    </tr>
                <?php   
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
