<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?php echo $title_content; ?></h1>  
   
</div>
    <div class="row">
        <div class="col-md-12 order-md-1">
            <form id="form_produk" method="post" action="<?php echo base_url();?>produk/save">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <div class="col-md-12 mb-3">
                        <label for="kode_produk">Kode Produk <span class="text-muted"></span></label>
                        <input type="text" class="form-control" id="kode_produk" name="kode_produk" placeholder="Kode Produk" <?php if(isset($produks)){ echo 'value="'.$produks['kode_produk'].'"'; } else{ echo 'value="'.$auto_produk_code.'"'; } ?> readonly>
                    </div>
          
                    <div class="col-md-12 mb-3">
                        <?php if(isset($produks)){
                            // Hide UUId Brand    
                        ?>
                            <input type="hidden" class="form-control" id="uuid_produk" name="uuid_produk" <?php echo 'value="'.$produks['uuid_produk'].'"'; ?>>
                        <?php }?>
                        <label for="nama_produk">Nama<span class="text-muted"></span></label>
                        <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Nama"  <?php if(isset($produks)){ echo 'value="'.$produks['nama_produk'].'"'; } ?>>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="kategori">Tiper Paket<span class="text-muted"></span></label>
                        <select class="form-control" id="paket_tipe" name="paket_tipe">
                            <option value="">Pilih Tipe</option>
                            <option value="1" <?php  if(isset($produks)&& $produks['package']==1){ echo "selected='selected'"; } ?>>Set</option>
                            <option value="2" <?php  if(isset($produks)&& $produks['package']==2){ echo "selected='selected'"; } ?>>Item</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="col-md-12 mb-3">
                        <label for="harga">Harga <span class="text-muted"></span></label>
                        <input type="text" class="form-control" id="harga" name="harga" placeholder="Harga" <?php if(isset($produks)){ echo 'value="'.$produks['harga'].'"'; } ?>>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="deskripsi">Deskripsi <span class="text-muted"></span></label>
                        <textarea class="form-control" id="deskripsi" name="deskripsi"> <?php if(isset($produks)){ echo $produks['deskripsi']; }?> </textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3">
                    <h4 class="mb-3">Item In Produk</h4>
                    <div class="row">
                        <div class="col-md-2">
                            <h5>Search Barang</h5>
                        </div>
                        <div class="col-md-5 pt-1">
                            <select class="js-example-basic-single form-control col-md-12 " id="barang_add" name="barang_add">
                                
                            </select>
                        </div>
                        <div class="col-md-2 pl-0 pt-1 mb-2">
                            <div class="btn-toolbar">
                                <div class="btn-group mr-2">
                                <!-- href="<?php //echo base_url('produk/add'); ?>" -->
                                    <span  class="btn btn-sm btn-outline-secondary addBarang">Add</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-sm barang_list">
                        <!-- <thead> -->
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Brand</th>
                                <th>Kategori</th>
                                <th></th>
                            </tr>
                        <!-- <thead>
                        <tbody> -->
                            <?php 
                            if(isset($barang_produk)){

                                $no=1;
                                foreach ($barang_produk->result_array() as $r_barang_produk) {
                            ?>  
                                <tr id="<?php echo $r_barang_produk['uuid_barang']; ?>">
                                    <td><?php echo $r_barang_produk['kode_barang']; ?></td>
                                    <td><?php echo $r_barang_produk['nama_barang']; ?></td>
                                    <td><?php echo $r_barang_produk['nama_brand']; ?></td>
                                    <td><?php echo $r_barang_produk['nama_kategori']; ?></td>
                                    <td><a class="deleteBtnProduct">X</a></td>
                                </tr>  
                            <?php
                                    }
                                }
                            ?>
                        <!-- </tbody> -->
                    </table>
                </div>
            </div>
                <hr class="col-md-12 mb-4">
                    <button class="btn btn-primary btn-lg btn-block btn-outline-secondary" type="submit">Submit</button>
                <hr class="mb-12">
            </form>
        </div>
    </div>
</div>
